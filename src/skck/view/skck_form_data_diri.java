/*    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        radioJk = new javax.swing.ButtonGroup();
        radioSp = new javax.swing.ButtonGroup();
        radioKewarganegaraan = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jProgressBar1 = new javax.swing.JProgressBar();
        btnsatwil = new javax.swing.JButton();
        btndata = new javax.swing.JButton();
        btnhubkel = new javax.swing.JButton();
        btnpend = new javax.swing.JButton();
        btnperpid = new javax.swing.JButton();
        btncirfis = new javax.swing.JButton();
        btnlamp = new javax.swing.JButton();
        btnket = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtNama = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtTempat = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jLabel6 = new javax.swing.JLabel();
        jRadioButton3 = new javax.swing.JRadioButton();
        jRadioButton4 = new javax.swing.JRadioButton();
        jLabel7 = new javax.swing.JLabel();
        jRadioButton5 = new javax.swing.JRadioButton();
        jRadioButton6 = new javax.swing.JRadioButton();
        jLabel8 = new javax.swing.JLabel();
        comboTypeidentitas = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtPekerjaan = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtAlamat = new javax.swing.JTextArea();
        jLabel12 = new javax.swing.JLabel();
        comboAgama = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        comboPropinsi = new javax.swing.JComboBox<>();
        jLabel14 = new javax.swing.JLabel();
        comboKabkota = new javax.swing.JComboBox<>();
        jLabel15 = new javax.swing.JLabel();
        comboKecamatan = new javax.swing.JComboBox<>();
        txtNotelp = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txtNopaspor = new javax.swing.JTextField();
        txtNoidentitas = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        comboKelurahan = new javax.swing.JComboBox<>();
        labelFoto = new javax.swing.JLabel();
        txtTanggal = new com.toedter.calendar.JDateChooser();
        btnBatal = new javax.swing.JButton();
        btnKembali = new javax.swing.JButton();
        btnLanjut = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel1.setText("Form. Pendaftaran");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        jProgressBar1.setValue(25);
        getContentPane().add(jProgressBar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 880, 20));

        btnsatwil.setText("Satwil");
        btnsatwil.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnsatwil.setEnabled(false);
        btnsatwil.setFocusPainted(false);
        btnsatwil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsatwilActionPerformed(evt);
            }
        });
        getContentPane().add(btnsatwil, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 110, -1));

        btndata.setText("Data Pribadi");
        btndata.setAutoscrolls(true);
        btndata.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                btndataFocusGained(evt);
            }
        });
        btndata.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndataActionPerformed(evt);
            }
        });
        getContentPane().add(btndata, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 40, 110, -1));

        btnhubkel.setText("Hub. Keluarga");
        btnhubkel.setEnabled(false);
        btnhubkel.setFocusPainted(false);
        btnhubkel.setFocusable(false);
        btnhubkel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnhubkelActionPerformed(evt);
            }
        });
        getContentPane().add(btnhubkel, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 40, 110, -1));

        btnpend.setText("Pendidikan");
        btnpend.setEnabled(false);
        btnpend.setFocusPainted(false);
        btnpend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnpendActionPerformed(evt);
            }
        });
        getContentPane().add(btnpend, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 40, 110, -1));

        btnperpid.setText("Perkara Pidana");
        btnperpid.setEnabled(false);
        btnperpid.setFocusPainted(false);
        btnperpid.setFocusable(false);
        btnperpid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnperpidActionPerformed(evt);
            }
        });
        getContentPane().add(btnperpid, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 40, 110, -1));

        btncirfis.setText("Ciri Fisik");
        btncirfis.setEnabled(false);
        btncirfis.setFocusPainted(false);
        btncirfis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncirfisActionPerformed(evt);
            }
        });
        getContentPane().add(btncirfis, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 40, 110, -1));

        btnlamp.setText("Lampiran");
        btnlamp.setEnabled(false);
        btnlamp.setFocusPainted(false);
        btnlamp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnlampActionPerformed(evt);
            }
        });
        getContentPane().add(btnlamp, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 40, 110, -1));

        btnket.setText("Keterangan");
        btnket.setEnabled(false);
        btnket.setFocusPainted(false);
        btnket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnketActionPerformed(evt);
            }
        });
        getContentPane().add(btnket, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 40, 110, -1));

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("Tgl. Lahir");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 60, -1, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Foto");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 10, -1, -1));
        jPanel1.add(txtNama, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 30, 590, -1));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Kewarganegaraan");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 110, -1, -1));
        jPanel1.add(txtTempat, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 80, 370, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("Tempat Lahir");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 60, -1, -1));

        radioJk.add(jRadioButton1);
        jRadioButton1.setText("PEREMPUAN");
        jRadioButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jRadioButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 130, -1, -1));

        radioSp.add(jRadioButton2);
        jRadioButton2.setText("BELUM KAWIN");
        jRadioButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jRadioButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 130, -1, -1));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setText("No Telp./ HP");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 160, -1, -1));

        radioJk.add(jRadioButton3);
        jRadioButton3.setText("LAKI-LAKI");
        jRadioButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jRadioButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, -1, -1));

        radioKewarganegaraan.add(jRadioButton4);
        jRadioButton4.setText("WNA");
        jPanel1.add(jRadioButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 130, -1, -1));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel7.setText("Status Perkawinan");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 110, -1, -1));

        radioSp.add(jRadioButton5);
        jRadioButton5.setText("KAWIN");
        jPanel1.add(jRadioButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 130, -1, -1));

        radioKewarganegaraan.add(jRadioButton6);
        jRadioButton6.setText("WNI");
        jPanel1.add(jRadioButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 130, -1, -1));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel8.setText("Jenis Kelamin");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 110, -1, -1));

        jPanel1.add(comboTypeidentitas, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 530, 70, -1));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel9.setText("No. Paspor");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 510, -1, 10));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel10.setText("Pekerjaan");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 160, -1, -1));
        jPanel1.add(txtPekerjaan, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 180, 180, -1));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel11.setText("Agama");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 160, -1, -1));

        txtAlamat.setColumns(20);
        txtAlamat.setLineWrap(true);
        txtAlamat.setRows(5);
        jPanel1.add(txtAlamat, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 240, 590, 50));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel12.setText("Alamat (Saat Ini)");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 220, -1, -1));

        jPanel1.add(comboAgama, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 180, 160, -1));

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel13.setText("Provinsi (Saat Ini)");
        jPanel1.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 300, -1, -1));

        comboPropinsi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboPropinsiActionPerformed(evt);
            }
        });
        jPanel1.add(comboPropinsi, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 320, 590, -1));

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel14.setText("Kabupaten / Kota (Saat Ini)");
        jPanel1.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 350, -1, -1));

        jPanel1.add(comboKabkota, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 370, 590, -1));

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel15.setText("Kecamatan (Saat Ini)");
        jPanel1.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 400, -1, -1));

        jPanel1.add(comboKecamatan, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 420, 590, -1));
        jPanel1.add(txtNotelp, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 180, 190, -1));

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel16.setText("No. Identitas");
        jPanel1.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 510, -1, -1));
        jPanel1.add(txtNopaspor, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 530, 230, -1));
        jPanel1.add(txtNoidentitas, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 530, 230, -1));

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel17.setText("Nama  Lengkap");
        jPanel1.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, -1, -1));

        jButton1.setText("PILIH FOTO");
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 190, -1, -1));

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel18.setText("Kelurahan (Saat Ini)");
        jPanel1.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 450, -1, -1));

        jPanel1.add(comboKelurahan, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 470, 590, -1));
        jPanel1.add(labelFoto, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 34, 100, 140));
        jPanel1.add(txtTanggal, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 80, 190, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, 880, 570));

        btnBatal.setBackground(java.awt.Color.red);
        btnBatal.setText("Batalkan");
        btnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBatalActionPerformed(evt);
            }
        });
        getContentPane().add(btnBatal, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 670, -1, -1));

        btnKembali.setText("Kembali");
        btnKembali.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKembaliActionPerformed(evt);
            }
        });
        getContentPane().add(btnKembali, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 670, -1, -1));

        btnLanjut.setText("Lanjut");
        btnLanjut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLanjutActionPerformed(evt);
            }
        });
        getContentPane().add(btnLanjut, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 670, -1, -1));

        pack();
    }// </editor-fold>                        

    private void btnhubkelActionPerformed(java.awt.event.ActionEvent evt) {                                          
        
    }                                         

    private void btnBatalActionPerformed(java.awt.event.ActionEvent evt) {                                         
        this.dispose();
    }                                        

    private void btnsatwilActionPerformed(java.awt.event.ActionEvent evt) {                                          
        
    }                                         

    private void btnKembaliActionPerformed(java.awt.event.ActionEvent evt) {                                           
        this.dispose();
        skck_form_satwil obj = new skck_form_satwil();
        obj.setVisible(true);
    }                                          

    private void btndataActionPerformed(java.awt.event.ActionEvent evt) {                                        
        // TODO add your handling code here:
    }                                       

    private void btnperpidActionPerformed(java.awt.event.ActionEvent evt) {                                          
        
    }                                         

    private void btnpendActionPerformed(java.awt.event.ActionEvent evt) {                                        
        
    }                                       

    private void btncirfisActionPerformed(java.awt.event.ActionEvent evt) {                                          
       
    }                                         

    private void btnlampActionPerformed(java.awt.event.ActionEvent evt) {                                        
        
    }                                       

    private void btnketActionPerformed(java.awt.event.ActionEvent evt) {                                       
        
    }                                      

    private void btnLanjutActionPerformed(java.awt.event.ActionEvent evt) {                                          
        cbt.insert();
//        this.dispose();
//        skck_form_hubkeluarga obj = new skck_form_hubkeluarga();
//        obj.setVisible(true);
    }                                         

    private void btndataFocusGained(java.awt.event.FocusEvent evt) {                                    
        // TODO add your handling code here:
    }                                   

    private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {                                              
        // TODO add your handling code here:
    }                                             

    private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {                                              
        // TODO add your handling code here:
    }                                             

    private void jRadioButton3ActionPerformed(java.awt.event.ActionEvent evt) {                                              
        // TODO add your handling code here:
    }                                             

    private void comboPropinsiActionPerformed(java.awt.event.ActionEvent evt) {                                              
        // TODO add your handling code here:
        
    }                                             

    // Variables declaration - do not modify                     
    private javax.swing.JButton btnBatal;
    private javax.swing.JButton btnKembali;
    private javax.swing.JButton btnLanjut;
    private javax.swing.JButton btncirfis;
    private javax.swing.JButton btndata;
    private javax.swing.JButton btnhubkel;
    private javax.swing.JButton btnket;
    private javax.swing.JButton btnlamp;
    private javax.swing.JButton btnpend;
    private javax.swing.JButton btnperpid;
    private javax.swing.JButton btnsatwil;
    private javax.swing.JComboBox<String> comboAgama;
    private javax.swing.JComboBox<String> comboKabkota;
    private javax.swing.JComboBox<String> comboKecamatan;
    private javax.swing.JComboBox<String> comboKelurahan;
    private javax.swing.JComboBox<String> comboPropinsi;
    private javax.swing.JComboBox<String> comboTypeidentitas;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JRadioButton jRadioButton3;
    private javax.swing.JRadioButton jRadioButton4;
    private javax.swing.JRadioButton jRadioButton5;
    private javax.swing.JRadioButton jRadioButton6;
    private javax.swing.JLabel labelFoto;
    private javax.swing.ButtonGroup radioJk;
    private javax.swing.ButtonGroup radioKewarganegaraan;
    private javax.swing.ButtonGroup radioSp;
    private javax.swing.JTextArea txtAlamat;
    private javax.swing.JTextField txtNama;
    private javax.swing.JTextField txtNoidentitas;
    private javax.swing.JTextField txtNopaspor;
    private javax.swing.JTextField txtNotelp;
    private javax.swing.JTextField txtPekerjaan;
    private com.toedter.calendar.JDateChooser txtTanggal;
    private javax.swing.JTextField txtTempat;
    // End of variables declaration                   


/*
    kelompok 2 skck xii rpl 2
        anggota :
        1. Akmal Fauzi Salman
        2. Dina 
        3. Fajar Ramadhan
        4. Gan gan Ahmad 
        5. Rafli Santana
        6. Zhahra Sahira
        
*/
package skck.view;
import com.toedter.calendar.JDateChooser;
import java.io.File;
import skck.controller.controllerDatadiri;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.*;
import skck.koneksi.koneksi;

/**
 *
 * @author gu
 */
public class skck_form_data_diri extends javax.swing.JFrame {
controllerDatadiri cbt;
ButtonGroup buttonGroup1,buttonGroup2,buttonGroup3;
String jk;
DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
Date newDate = new Date();

    /**
     * Creates new form skck_form_daftar_satwil
     */
    public skck_form_data_diri() {
        initComponents();
        cbt = new controllerDatadiri(this);
        
        DocumentFilter filter = new UppercaseDocumentFilter();
        AbstractDocument txtNamaDoc = (AbstractDocument) txtNama.getDocument();
        txtNamaDoc.setDocumentFilter(filter);
        AbstractDocument txtTempatDoc = (AbstractDocument) txtTempat.getDocument();
        txtTempatDoc.setDocumentFilter(filter);
        AbstractDocument txtPekerjaanDoc = (AbstractDocument) txtPekerjaan.getDocument();
        txtPekerjaanDoc.setDocumentFilter(filter);
        AbstractDocument txtAlamatDoc = (AbstractDocument) txtAlamat.getDocument();
        txtAlamatDoc.setDocumentFilter(filter);
        
        radio1.setActionCommand("LAKI-LAKI");
        radio2.setActionCommand("PEREMPUAN");
        radio3.setActionCommand("KAWIN");
        radio4.setActionCommand("BELUM KAWIN");
        radio5.setActionCommand("WNI");
        radio6.setActionCommand("WNA");
        
        buttonGroup1=new ButtonGroup();
        buttonGroup1.add(radio1);
        buttonGroup1.add(radio2);
        buttonGroup2=new ButtonGroup();
        buttonGroup2.add(radio3);
        buttonGroup2.add(radio4);
        buttonGroup3=new ButtonGroup();
        buttonGroup3.add(radio5);
        buttonGroup3.add(radio6);
        
        label.setVisible(false);
        label1.setVisible(false);
        label2.setVisible(false);
        label3.setVisible(false);

        tampil_agama();
        tampil_propinsi();
        tampil_kabkota();
        tampil_kecamatan();
        tampil_kelurahan();
        tampil_type();
    }
    
    public void tampil_agama()
    {
        try {
        Connection con = koneksi.connection();
        Statement stt = con.createStatement();
        String sql = "select agama from agama order by id_agama asc";      // disini saya menampilkan NIM, anda dapat menampilkan
        ResultSet res = stt.executeQuery(sql);                                // yang anda ingin kan
        
        while(res.next()){
            Object[] ob = new Object[3];
            ob[0] = res.getString(1);
            
            comboAgama.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
        }
        res.close(); stt.close();
         
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void tampil_propinsi()
    {
        try {
        Connection con = koneksi.connection();
        Statement stt = con.createStatement();
        String sql = "select nama_propinsi from m_ipropinsi order by id_propinsi asc";      // disini saya menampilkan NIM, anda dapat menampilkan
        ResultSet res = stt.executeQuery(sql);                                // yang anda ingin kan
        
        while(res.next()){
            Object[] ob = new Object[3];
            ob[0] = res.getString(1);
            
            comboPropinsi.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
        }
        res.close(); stt.close();
         
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void tampil_kabkota()
    {
        try {
        Connection con = koneksi.connection();
        Statement stt = con.createStatement();
        String sql = "select nama_kabkota from m_ikabkota order by id_kabkota asc";      // disini saya menampilkan NIM, anda dapat menampilkan
        ResultSet res = stt.executeQuery(sql);                                // yang anda ingin kan
        
        while(res.next()){
            Object[] ob = new Object[3];
            ob[0] = res.getString(1);
            
            comboKabkota.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
        }
        res.close(); stt.close();
         
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void tampil_kecamatan()
    {
        try {
        Connection con = koneksi.connection();
        Statement stt = con.createStatement();
        String sql = "select nama_kecamatan from m_ikecamatan order by id_kecamatan asc";      // disini saya menampilkan NIM, anda dapat menampilkan
        ResultSet res = stt.executeQuery(sql);                                // yang anda ingin kan
        
        while(res.next()){
            Object[] ob = new Object[3];
            ob[0] = res.getString(1);
            
            comboKecamatan.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
        }
        res.close(); stt.close();
         
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void tampil_kelurahan()
    {
        try {
        Connection con = koneksi.connection();
        Statement stt = con.createStatement();
        String sql = "select nama_kelurahan from m_ikelurahan order by id_kelurahan asc";      // disini saya menampilkan NIM, anda dapat menampilkan
        ResultSet res = stt.executeQuery(sql);                                // yang anda ingin kan
        
        while(res.next()){
            Object[] ob = new Object[3];
            ob[0] = res.getString(1);
            
            comboKelurahan.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
        }
        res.close(); stt.close();
         
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void tampil_type()
    {
        try {
        Connection con = koneksi.connection();
        Statement stt = con.createStatement();
        String sql = "select type from type_identitas order by id_type asc";      // disini saya menampilkan NIM, anda dapat menampilkan
        ResultSet res = stt.executeQuery(sql);                                // yang anda ingin kan
        
        while(res.next()){
            Object[] ob = new Object[3];
            ob[0] = res.getString(1);
            
            comboTypeidentitas.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
        }
        res.close(); stt.close();
         
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    class UppercaseDocumentFilter extends DocumentFilter{
        public void insertString(DocumentFilter.FilterBypass fb, int offset, String text, AttributeSet attr) throws BadLocationException{
            fb.insertString(offset,text.toUpperCase(),attr);
        }
        public void replace(DocumentFilter.FilterBypass fb, int offset,int length, String text, AttributeSet attrs) throws BadLocationException{
            fb.replace(offset,length,text.toUpperCase(),attrs);
        }
    }          

    
    private void radio2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radio2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_radio2ActionPerformed

    private void radio4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radio4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_radio4ActionPerformed

    private void radio1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radio1ActionPerformed
    // TODO add your handling code here:
    }//GEN-LAST:event_radio1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        JFileChooser file = new JFileChooser();
        file.setCurrentDirectory(new File(System.getProperty("user.home")));
        //filter the files
        FileNameExtensionFilter filter = new FileNameExtensionFilter("*.Images","jpg");
        file.addChoosableFileFilter(filter);
        int result = file.showOpenDialog(null);
        //if the user click on open in Jfilechooser
        if(result == JFileChooser.APPROVE_OPTION){
            File selectedFile = file.getSelectedFile();
            String path = selectedFile.getAbsolutePath();
//            img = setFile(path);
//            canvas.setIcon(ResizeImage(path));
        }
        //if the user click cancel
        else if(result == JFileChooser.CANCEL_OPTION){
            System.out.println("No File Select");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(skck_form_data_diri.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(skck_form_data_diri.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(skck_form_data_diri.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(skck_form_data_diri.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new skck_form_data_diri().setVisible(true);
            }
        });
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        radioJk = new javax.swing.ButtonGroup();
        radioSp = new javax.swing.ButtonGroup();
        radioKewarganegaraan = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jProgressBar1 = new javax.swing.JProgressBar();
        btnsatwil = new javax.swing.JButton();
        btndata = new javax.swing.JButton();
        btnhubkel = new javax.swing.JButton();
        btnpend = new javax.swing.JButton();
        btnperpid = new javax.swing.JButton();
        btncirfis = new javax.swing.JButton();
        btnlamp = new javax.swing.JButton();
        btnket = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtNama = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtTempat = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        radio2 = new javax.swing.JRadioButton();
        radio4 = new javax.swing.JRadioButton();
        jLabel6 = new javax.swing.JLabel();
        radio1 = new javax.swing.JRadioButton();
        radio6 = new javax.swing.JRadioButton();
        jLabel7 = new javax.swing.JLabel();
        radio3 = new javax.swing.JRadioButton();
        radio5 = new javax.swing.JRadioButton();
        jLabel8 = new javax.swing.JLabel();
        comboTypeidentitas = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtPekerjaan = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtAlamat = new javax.swing.JTextArea();
        jLabel12 = new javax.swing.JLabel();
        comboAgama = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        comboPropinsi = new javax.swing.JComboBox<>();
        jLabel14 = new javax.swing.JLabel();
        comboKabkota = new javax.swing.JComboBox<>();
        jLabel15 = new javax.swing.JLabel();
        comboKecamatan = new javax.swing.JComboBox<>();
        txtNotelp = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txtNopaspor = new javax.swing.JTextField();
        txtNoidentitas = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        comboKelurahan = new javax.swing.JComboBox<>();
        canvas = new javax.swing.JLabel();
        txtTanggal = new com.toedter.calendar.JDateChooser();
        label = new javax.swing.JLabel();
        label1 = new javax.swing.JLabel();
        label2 = new javax.swing.JLabel();
        label3 = new javax.swing.JLabel();
        img = new javax.swing.JLabel();
        vali1 = new javax.swing.JLabel();
        btnBatal = new javax.swing.JButton();
        btnKembali = new javax.swing.JButton();
        btnLanjut = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel1.setText("Form. Pendaftaran");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        jProgressBar1.setValue(25);
        getContentPane().add(jProgressBar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 880, 20));

        btnsatwil.setText("Satwil");
        btnsatwil.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnsatwil.setEnabled(false);
        btnsatwil.setFocusPainted(false);
        btnsatwil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsatwilActionPerformed(evt);
            }
        });
        getContentPane().add(btnsatwil, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 110, -1));

        btndata.setText("Data Pribadi");
        btndata.setAutoscrolls(true);
        btndata.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                btndataFocusGained(evt);
            }
        });
        btndata.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndataActionPerformed(evt);
            }
        });
        getContentPane().add(btndata, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 40, 110, -1));

        btnhubkel.setText("Hub. Keluarga");
        btnhubkel.setEnabled(false);
        btnhubkel.setFocusPainted(false);
        btnhubkel.setFocusable(false);
        btnhubkel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnhubkelActionPerformed(evt);
            }
        });
        getContentPane().add(btnhubkel, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 40, 110, -1));

        btnpend.setText("Pendidikan");
        btnpend.setEnabled(false);
        btnpend.setFocusPainted(false);
        btnpend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnpendActionPerformed(evt);
            }
        });
        getContentPane().add(btnpend, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 40, 110, -1));

        btnperpid.setText("Perkara Pidana");
        btnperpid.setEnabled(false);
        btnperpid.setFocusPainted(false);
        btnperpid.setFocusable(false);
        btnperpid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnperpidActionPerformed(evt);
            }
        });
        getContentPane().add(btnperpid, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 40, 110, -1));

        btncirfis.setText("Ciri Fisik");
        btncirfis.setEnabled(false);
        btncirfis.setFocusPainted(false);
        btncirfis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncirfisActionPerformed(evt);
            }
        });
        getContentPane().add(btncirfis, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 40, 110, -1));

        btnlamp.setText("Lampiran");
        btnlamp.setEnabled(false);
        btnlamp.setFocusPainted(false);
        btnlamp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnlampActionPerformed(evt);
            }
        });
        getContentPane().add(btnlamp, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 40, 110, -1));

        btnket.setText("Keterangan");
        btnket.setEnabled(false);
        btnket.setFocusPainted(false);
        btnket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnketActionPerformed(evt);
            }
        });
        getContentPane().add(btnket, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 40, 110, -1));

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("Tgl. Lahir");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 60, -1, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Foto");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 10, -1, -1));
        jPanel1.add(txtNama, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 30, 590, -1));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Kewarganegaraan");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 110, -1, -1));
        jPanel1.add(txtTempat, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 80, 370, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("Tempat Lahir");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 60, -1, -1));

        radioJk.add(radio2);
        radio2.setText("PEREMPUAN");
        radio2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radio2ActionPerformed(evt);
            }
        });
        jPanel1.add(radio2, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 130, -1, -1));

        radioSp.add(radio4);
        radio4.setText("BELUM KAWIN");
        radio4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radio4ActionPerformed(evt);
            }
        });
        jPanel1.add(radio4, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 130, -1, -1));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setText("No Telp./ HP");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 160, -1, -1));

        radioJk.add(radio1);
        radio1.setText("LAKI-LAKI");
        radio1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radio1ActionPerformed(evt);
            }
        });
        jPanel1.add(radio1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, -1, -1));

        radioKewarganegaraan.add(radio6);
        radio6.setText("WNA");
        jPanel1.add(radio6, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 130, -1, -1));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel7.setText("Status Perkawinan");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 110, -1, -1));

        radioSp.add(radio3);
        radio3.setText("KAWIN");
        jPanel1.add(radio3, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 130, -1, -1));

        radioKewarganegaraan.add(radio5);
        radio5.setText("WNI");
        jPanel1.add(radio5, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 130, -1, -1));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel8.setText("Jenis Kelamin");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 110, -1, -1));

        jPanel1.add(comboTypeidentitas, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 530, 70, -1));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel9.setText("No. Paspor");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 510, -1, 10));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel10.setText("Pekerjaan");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 160, -1, -1));
        jPanel1.add(txtPekerjaan, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 180, 180, -1));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel11.setText("Agama");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 160, -1, -1));

        txtAlamat.setColumns(20);
        txtAlamat.setLineWrap(true);
        txtAlamat.setRows(5);
        jPanel1.add(txtAlamat, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 240, 590, 50));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel12.setText("Alamat (Saat Ini)");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 220, -1, -1));

        jPanel1.add(comboAgama, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 180, 160, -1));

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel13.setText("Provinsi (Saat Ini)");
        jPanel1.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 300, -1, -1));

        comboPropinsi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboPropinsiActionPerformed(evt);
            }
        });
        jPanel1.add(comboPropinsi, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 320, 590, -1));

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel14.setText("Kabupaten / Kota (Saat Ini)");
        jPanel1.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 350, -1, -1));

        jPanel1.add(comboKabkota, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 370, 590, -1));

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel15.setText("Kecamatan (Saat Ini)");
        jPanel1.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 400, -1, -1));

        jPanel1.add(comboKecamatan, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 420, 590, -1));
        jPanel1.add(txtNotelp, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 180, 190, -1));

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel16.setText("No. Identitas");
        jPanel1.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 510, -1, -1));
        jPanel1.add(txtNopaspor, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 530, 230, -1));
        jPanel1.add(txtNoidentitas, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 530, 230, -1));

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel17.setText("Nama  Lengkap");
        jPanel1.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, -1, -1));

        jButton1.setText("PILIH FOTO");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 190, -1, -1));

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel18.setText("Kelurahan (Saat Ini)");
        jPanel1.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 450, -1, -1));

        jPanel1.add(comboKelurahan, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 470, 590, -1));
        jPanel1.add(canvas, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 40, 100, 140));
        jPanel1.add(txtTanggal, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 80, 190, -1));
        jPanel1.add(label, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 320, 150, 20));
        jPanel1.add(label1, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 240, 150, 20));
        jPanel1.add(label2, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 320, 150, 20));
        jPanel1.add(label3, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 350, 150, 20));
        jPanel1.add(img, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 34, 100, 140));
        jPanel1.add(vali1, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 200, 190, 20));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, 880, 570));

        btnBatal.setBackground(java.awt.Color.red);
        btnBatal.setText("Batalkan");
        btnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBatalActionPerformed(evt);
            }
        });
        getContentPane().add(btnBatal, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 670, -1, -1));

        btnKembali.setText("Kembali");
        btnKembali.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKembaliActionPerformed(evt);
            }
        });
        getContentPane().add(btnKembali, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 670, -1, -1));

        btnLanjut.setText("Lanjut");
        btnLanjut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLanjutActionPerformed(evt);
            }
        });
        getContentPane().add(btnLanjut, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 670, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnhubkelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnhubkelActionPerformed
        
    }//GEN-LAST:event_btnhubkelActionPerformed

    private void btnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBatalActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnBatalActionPerformed

    private void btnsatwilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsatwilActionPerformed
        
    }//GEN-LAST:event_btnsatwilActionPerformed

    private void btnKembaliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnKembaliActionPerformed
        this.dispose();
        skck_form_satwil obj = new skck_form_satwil();
        obj.setVisible(true);
    }//GEN-LAST:event_btnKembaliActionPerformed

    private void btndataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndataActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btndataActionPerformed

    private void btnperpidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnperpidActionPerformed
        
    }//GEN-LAST:event_btnperpidActionPerformed

    private void btnpendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnpendActionPerformed
        
    }//GEN-LAST:event_btnpendActionPerformed

    private void btncirfisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncirfisActionPerformed
       
    }//GEN-LAST:event_btncirfisActionPerformed

    private void btnlampActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnlampActionPerformed
        
    }//GEN-LAST:event_btnlampActionPerformed

    private void btnketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnketActionPerformed
        
    }//GEN-LAST:event_btnketActionPerformed

    private void btnLanjutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLanjutActionPerformed
        label.setText(buttonGroup1.getSelection().getActionCommand());
        label1.setText(buttonGroup2.getSelection().getActionCommand());
        label2.setText(buttonGroup3.getSelection().getActionCommand());
        label3.setText(dateFormat.format(newDate));
          if(!txtNotelp.getText().matches("[0-9]*")){
              vali1.setText("Masukan Harus Berupa Angka!");
              vali1.setVisible(true);
          }else{
              vali1.setVisible(false);
              cbt.insert();
        this.dispose();
        skck_form_hubkeluarga obj = new skck_form_hubkeluarga();
        obj.setVisible(true);
          }
    }//GEN-LAST:event_btnLanjutActionPerformed

    private void btndataFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_btndataFocusGained
    }//GEN-LAST:event_btndataFocusGained

    private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButton1ActionPerformed

    private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButton2ActionPerformed

    private void jRadioButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButton3ActionPerformed

    private void comboPropinsiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboPropinsiActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_comboPropinsiActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBatal;
    private javax.swing.JButton btnKembali;
    private javax.swing.JButton btnLanjut;
    private javax.swing.JButton btncirfis;
    private javax.swing.JButton btndata;
    private javax.swing.JButton btnhubkel;
    private javax.swing.JButton btnket;
    private javax.swing.JButton btnlamp;
    private javax.swing.JButton btnpend;
    private javax.swing.JButton btnperpid;
    private javax.swing.JButton btnsatwil;
    private javax.swing.JLabel canvas;
    private javax.swing.JComboBox<String> comboAgama;
    private javax.swing.JComboBox<String> comboKabkota;
    private javax.swing.JComboBox<String> comboKecamatan;
    private javax.swing.JComboBox<String> comboKelurahan;
    private javax.swing.JComboBox<String> comboPropinsi;
    private javax.swing.JComboBox<String> comboTypeidentitas;
    private javax.swing.JLabel img;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JLabel label;
    private javax.swing.JLabel label1;
    private javax.swing.JLabel label2;
    private javax.swing.JLabel label3;
    private javax.swing.JRadioButton radio1;
    private javax.swing.JRadioButton radio2;
    private javax.swing.JRadioButton radio3;
    private javax.swing.JRadioButton radio4;
    private javax.swing.JRadioButton radio5;
    private javax.swing.JRadioButton radio6;
    private javax.swing.ButtonGroup radioJk;
    private javax.swing.ButtonGroup radioKewarganegaraan;
    private javax.swing.ButtonGroup radioSp;
    private javax.swing.JTextArea txtAlamat;
    private javax.swing.JTextField txtNama;
    private javax.swing.JTextField txtNoidentitas;
    private javax.swing.JTextField txtNopaspor;
    private javax.swing.JTextField txtNotelp;
    private javax.swing.JTextField txtPekerjaan;
    private com.toedter.calendar.JDateChooser txtTanggal;
    private javax.swing.JTextField txtTempat;
    private javax.swing.JLabel vali1;
    // End of variables declaration//GEN-END:variables
    public JTextField getTxtNama(){
        return txtNama;
    }
    public JTextField getTxtTempat(){
        return txtTempat;
    }
    public JDateChooser getTxtTanggal(){
        return txtTanggal;
    }
    public ButtonGroup getTxtJk(){
        return radioJk;
    }
    public ButtonGroup getTxtSp(){
        return radioSp;
    }
    public ButtonGroup getTxtKewarganegaraan(){
        return radioKewarganegaraan;
    }
    public JComboBox<String> getTxtAgama(){
            return comboAgama;
    }
    public JTextField getTxtPekerjaan(){
        return txtPekerjaan;
    }
    public JTextField getTxtNotelp(){
        return txtNotelp;
    }
    public JTextArea getTxtAlamat(){
        return txtAlamat;
    }
    public JComboBox<String> getTxtPropinsi(){
            return comboPropinsi;
    }
    public JComboBox<String> getTxtKabkota(){
            return comboKabkota;
    }
    public JComboBox<String> getTxtKecamatan(){
            return comboKecamatan;
    }
    public JComboBox<String> getTxtKelurahan(){
            return comboKelurahan;
    }
    public JComboBox<String> getTxtTypeidentitas(){
            return comboTypeidentitas;
    }
    public JTextField getTxtNoidentitas(){
        return txtNoidentitas;
    }
    public JTextField getTxtNopaspor(){
        return txtNopaspor;
    }
    public JLabel getTxtFoto(){
        return canvas;
    }
    public JLabel getJK(){
        return label;
    }
    public JLabel getJKK(){
        return label1;
    }
    public JLabel getJKKK(){
        return label2;
    }
    public JLabel getJKKKK(){
        return label3;
    }
}