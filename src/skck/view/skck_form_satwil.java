/*
    kelompok 2 skck xii rpl 2
        anggota :
        1. Akmal Fauzi Salman
        2. Dina 
        3. Fajar Ramadhan
        4. Gan gan Ahmad 
        5. Rafli Santana
        6. Zhahra Sahira
        
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.view;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import skck.controller.satwilController;
import skck.event.satwilListener;
import skck.koneksi.koneksi;
import skck.model.satwilModel;

/**
 *
 * @author gu
 */
public class skck_form_satwil extends javax.swing.JFrame implements satwilListener{

    private satwilModel pm;
    private satwilController pc;
    
    public skck_form_satwil() {
        pm = new satwilModel();
        pc = new satwilController();
        pm.setSatwilListener(this);
        pc.setSatwilModel(pm);
        
        initComponents();
        
        tampilKeperluan();
    }
    
    public void tampilKeperluan(){
        
        try{
            Connection con = koneksi.connection();
            Statement stt = con.createStatement();
            String sql = "SELECT nama_keperluan FROM keperluan";      
            ResultSet res = stt.executeQuery(sql);                               
        
        while(res.next()){
            Object[] ob = new Object[3];
            ob[0] = res.getString(1);
            
            comboKeperluan.addItem((String) ob[0]);                                      
        }
        res.close();
        stt.close();
            
        } catch (Exception e){
             System.out.println(e.getMessage());
        }
    }
    
    public void tampilSatwil() {
        try{
            Connection con = koneksi.connection();
            Statement stt = con.createStatement();
            String sql = "SELECT jenis_keperluan FROM keperluan WHERE nama_keperluan='"+comboKeperluan.getSelectedItem()+"' ";      
            ResultSet res = stt.executeQuery(sql);                               
        
        while(res.next()){
            Object[] ob = new Object[3];
            ob[0] = res.getString(1);
            
            try{
                Connection con1 = koneksi.connection();
                Statement stt1 = con1.createStatement();
                String sql1 = "SELECT nama_satwil FROM satwil WHERE jenis_satwil='"+(String) ob[0]+"' ";// disini saya menampilkan NIM, anda dapat menampilkan
                ResultSet res1 = stt1.executeQuery(sql1);                                // yang anda ingin kan

            while(res1.next()){
                Object[] ob1 = new Object[3];
                ob1[0] = res1.getString(1);
                
                comboSatwil.removeAllItems();
                comboSatwil.addItem((String) ob1[0]);   
                                    // fungsi ini bertugas menampung isi dari database
            }
            res1.close();stt1.close();

            } catch (Exception e){
                 System.out.println(e.getMessage());
            }
        }
        res.close();
        stt.close();
            
        } catch (Exception e){
             System.out.println(e.getMessage());
        }
        
    }
    
    public void tampilProvinsi() {
        try{
            Connection con = koneksi.connection();
            Statement stt = con.createStatement();
            String sql = "SELECT provinsi FROM satwil WHERE nama_satwil='"+comboSatwil.getSelectedItem()+"' ";      
            ResultSet res = stt.executeQuery(sql);                               
        
        while(res.next()){
            Object[] ob = new Object[3];
            ob[0] = res.getString(1);
                       
            comboProvinsi.removeAllItems();
            comboProvinsi.addItem((String) ob[0]);
        }
        res.close();
        stt.close();
            
        } catch (Exception e){
             System.out.println(e.getMessage());
        }
        
    }
    
    public void tampilKota() {
        try{
            Connection con = koneksi.connection();
            Statement stt = con.createStatement();
            String sql = "SELECT kota FROM kota WHERE provinsi='"+comboProvinsi.getSelectedItem()+"' ";      
            ResultSet res = stt.executeQuery(sql);                               
        
        while(res.next()){
            Object[] ob = new Object[3];
            ob[0] = res.getString(1);
                       
            comboKota.removeAllItems();
            comboKota.addItem((String) ob[0]);
        }
        res.close();
        stt.close();
            
        } catch (Exception e){
             System.out.println(e.getMessage());
        }
        
    }
    
    public void tampilKec() {
        try{
            Connection con = koneksi.connection();
            Statement stt = con.createStatement();
            String sql = "SELECT kec FROM kecamatan WHERE kota='"+comboKota.getSelectedItem()+"' ";      
            ResultSet res = stt.executeQuery(sql);                               
        
        while(res.next()){
            Object[] ob = new Object[3];
            ob[0] = res.getString(1);
                       
            comboKec.removeAllItems();
            comboKec.addItem((String) ob[0]);
        }
        res.close();
        stt.close();
            
        } catch (Exception e){
             System.out.println(e.getMessage());
        }
        
    }
    
    public void tampilKel() {
        try{
            Connection con = koneksi.connection();
            Statement stt = con.createStatement();
            String sql = "SELECT kel FROM kelurahan WHERE kec='"+comboKec.getSelectedItem()+"' ";      
            ResultSet res = stt.executeQuery(sql);                               
        
        while(res.next()){
            Object[] ob = new Object[3];
            ob[0] = res.getString(1);
                       
            comboKel.removeAllItems();
            comboKel.addItem((String) ob[0]);
        }
        res.close();
        stt.close();
            
        } catch (Exception e){
             System.out.println(e.getMessage());
        }
        
    }

    public JComboBox<String> getComboKec() {
        return comboKec;
    }

    public JComboBox<String> getComboKel() {
        return comboKel;
    }

    public JComboBox<String> getComboKeperluan() {
        return comboKeperluan;
    }

    public JComboBox<String> getComboKota() {
        return comboKota;
    }

    public JComboBox<String> getComboProvinsi() {
        return comboProvinsi;
    }

    public JComboBox<String> getComboSatwil() {
        return comboSatwil;
    }

    public JTextField getTxtAlamat() {
        return txtAlamat;
    }
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jProgressBar1 = new javax.swing.JProgressBar();
        btnsatwil = new javax.swing.JButton();
        btndata = new javax.swing.JButton();
        btnhubkel = new javax.swing.JButton();
        btnpend = new javax.swing.JButton();
        btnperpid = new javax.swing.JButton();
        btncirfis = new javax.swing.JButton();
        btnlamp = new javax.swing.JButton();
        btnket = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        comboSatwil = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        comboKel = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txtAlamat = new javax.swing.JTextField();
        comboKeperluan = new javax.swing.JComboBox<>();
        comboProvinsi = new javax.swing.JComboBox<>();
        comboKota = new javax.swing.JComboBox<>();
        comboKec = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        btnBatal = new javax.swing.JButton();
        btnKembali = new javax.swing.JButton();
        btnLanjut = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        setMinimumSize(new java.awt.Dimension(900, 644));
        setSize(new java.awt.Dimension(900, 644));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel1.setText("Form. Pendaftaran");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        jProgressBar1.setToolTipText("");
        jProgressBar1.setValue(12);
        getContentPane().add(jProgressBar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 880, 20));

        btnsatwil.setText("Satwil");
        btnsatwil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsatwilActionPerformed(evt);
            }
        });
        getContentPane().add(btnsatwil, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 110, -1));

        btndata.setText("Data Pribadi");
        btndata.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btndata.setEnabled(false);
        btndata.setFocusPainted(false);
        btndata.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndataActionPerformed(evt);
            }
        });
        getContentPane().add(btndata, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 40, 110, -1));

        btnhubkel.setText("Hub. Keluarga");
        btnhubkel.setEnabled(false);
        btnhubkel.setFocusPainted(false);
        btnhubkel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnhubkelActionPerformed(evt);
            }
        });
        getContentPane().add(btnhubkel, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 40, 110, -1));

        btnpend.setText("Pendidikan");
        btnpend.setEnabled(false);
        btnpend.setFocusPainted(false);
        btnpend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnpendActionPerformed(evt);
            }
        });
        getContentPane().add(btnpend, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 40, 110, -1));

        btnperpid.setText("Perkara Pidana");
        btnperpid.setEnabled(false);
        btnperpid.setFocusPainted(false);
        btnperpid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnperpidActionPerformed(evt);
            }
        });
        getContentPane().add(btnperpid, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 40, 110, -1));

        btncirfis.setText("Ciri Fisik");
        btncirfis.setEnabled(false);
        btncirfis.setFocusPainted(false);
        btncirfis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncirfisActionPerformed(evt);
            }
        });
        getContentPane().add(btncirfis, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 40, 110, -1));

        btnlamp.setText("Lampiran");
        btnlamp.setEnabled(false);
        btnlamp.setFocusPainted(false);
        btnlamp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnlampActionPerformed(evt);
            }
        });
        getContentPane().add(btnlamp, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 40, 110, -1));

        btnket.setText("Keterangan");
        btnket.setEnabled(false);
        btnket.setFocusPainted(false);
        btnket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnketActionPerformed(evt);
            }
        });
        getContentPane().add(btnket, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 40, 110, -1));

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Calibri", 1, 10)); // NOI18N
        jLabel2.setText("Kabupaten / Kota (Sesuai KTP)");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 310, -1, -1));

        jLabel3.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel3.setText("A. MABES POLRI");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 30, -1, -1));

        jLabel4.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel4.setText("B. POLDA");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 120, -1, -1));

        jLabel5.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel5.setText("C. POLRES");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 220, -1, -1));

        jLabel6.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel6.setText("D. POLSEK");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 310, -1, -1));

        jLabel29.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel29.setText("- Pencalonan Presiden dan Wakil Presiden");
        jPanel1.add(jLabel29, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 40, -1, -1));

        jLabel30.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel30.setText("- Pembuatan Visa");
        jPanel1.add(jLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 60, -1, -1));

        jLabel31.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel31.setText("- Pencalonan Anggota Legislatif, Eksekutif, Yudikatif, dan Lembaga Pemerintahan Tingkat Pusat");
        jPanel1.add(jLabel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 50, 390, -1));

        jLabel32.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel32.setText("- Ijin Tetap Tinggal di Luar Negeri");
        jPanel1.add(jLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 70, -1, -1));

        jLabel33.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel33.setText("- Naturalisasi Kewarganegaraan");
        jPanel1.add(jLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 80, -1, -1));

        jLabel34.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel34.setText("- Adopsi Anak Bagi Pemohon WNA");
        jPanel1.add(jLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 90, -1, -1));

        jLabel35.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel35.setText("- Melanjutkan Sekolah Luar Negeri");
        jPanel1.add(jLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 100, -1, -1));

        jLabel36.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel36.setText("- Melamar Pekerjaan");
        jPanel1.add(jLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 130, -1, -1));

        jLabel37.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel37.setText("- Warga Negara Indonesia (WNI) Yang Akan Bekerja ke Luar Negeri");
        jPanel1.add(jLabel37, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 150, -1, -1));

        jLabel38.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel38.setText("- Memperoleh Paspor atau Visa");
        jPanel1.add(jLabel38, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 140, 390, -1));

        jLabel39.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel39.setText("- Menjadi Notaris");
        jPanel1.add(jLabel39, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 160, -1, -1));

        jLabel40.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel40.setText("- Pencalonan Pejabat Publik");
        jPanel1.add(jLabel40, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 170, -1, -1));

        jLabel41.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel41.setText("- Melanjutkan Sekolah");
        jPanel1.add(jLabel41, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 180, -1, -1));

        jLabel42.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel42.setText("- Pencalonan Kepala Daerah Tingkat Provinsi");
        jPanel1.add(jLabel42, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 200, -1, -1));

        jLabel43.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel43.setText("- Pencalonan Anggota Legislatif Tingkat Provinsi");
        jPanel1.add(jLabel43, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 190, -1, -1));

        jLabel44.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel44.setText("- Pencalonan Anggota Legislatif Tingkat Kabupaten/Kota");
        jPanel1.add(jLabel44, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 230, -1, -1));

        jLabel45.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel45.setText("- Melamar Sebagai Anggota TNI/POLRI");
        jPanel1.add(jLabel45, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 250, -1, -1));

        jLabel46.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel46.setText("- Melamar Sebagai PNS");
        jPanel1.add(jLabel46, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 240, 390, -1));

        jLabel47.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel47.setText("- Pencalonan Pejabat Publik");
        jPanel1.add(jLabel47, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 260, -1, -1));

        jLabel48.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel48.setText("- Kepemilikan Senjata Api ");
        jPanel1.add(jLabel48, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 270, -1, -1));

        jLabel49.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel49.setText("- Melamar Pekerjaan");
        jPanel1.add(jLabel49, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 280, -1, -1));

        jLabel50.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel50.setText("- Pencalonan Kepala Daerah Tingkat Kabupaten/Kota");
        jPanel1.add(jLabel50, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 290, -1, -1));

        jLabel51.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel51.setText("- Melamar Pekerjaan");
        jPanel1.add(jLabel51, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 320, -1, -1));

        jLabel52.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel52.setText("- Pencalonan Sekertaris Desa");
        jPanel1.add(jLabel52, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 340, -1, -1));

        jLabel53.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel53.setText("- Pencalonan Kepala Desa");
        jPanel1.add(jLabel53, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 330, 390, -1));

        jLabel54.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel54.setText("- Pindah Alamat");
        jPanel1.add(jLabel54, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 350, -1, -1));

        jLabel55.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel55.setText("- Melanjutkan Sekolah");
        jPanel1.add(jLabel55, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 360, -1, -1));

        jLabel7.setFont(new java.awt.Font("Calibri", 1, 11)); // NOI18N
        jLabel7.setText("Informasi Keperluan Pembuatan SKCK dan Tingkat Kewenangan Kesatuan Wilayah  ");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 10, -1, -1));

        comboSatwil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboSatwilActionPerformed(evt);
            }
        });
        jPanel1.add(comboSatwil, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 120, 400, -1));

        jLabel8.setFont(new java.awt.Font("Calibri", 1, 10)); // NOI18N
        jLabel8.setText("Silahkan pilih jenis keperluan pembuatan SKCK sesuai dengan tingkat kewenangan yang diperlukan");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, -1, -1));

        jPanel1.add(comboKel, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 430, 400, -1));

        jLabel9.setFont(new java.awt.Font("Calibri", 1, 10)); // NOI18N
        jLabel9.setText("Silahkan pilih kesatuan wilayah untuk proses pembuatan dan pengambilan SKCK (Sesuai KTP)");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 100, -1, -1));

        jLabel10.setFont(new java.awt.Font("Calibri", 1, 10)); // NOI18N
        jLabel10.setText("Alamat (Sesuai KTP)");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 190, -1, -1));

        jLabel12.setFont(new java.awt.Font("Calibri", 1, 10)); // NOI18N
        jLabel12.setText("Provinsi (Sesuai KTP)");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 260, -1, -1));

        jLabel13.setFont(new java.awt.Font("Calibri", 1, 10)); // NOI18N
        jLabel13.setText("Kecamatan (Sesuai KTP)");
        jPanel1.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 360, -1, -1));

        jLabel14.setFont(new java.awt.Font("Calibri", 1, 10)); // NOI18N
        jLabel14.setText("Kelurahan (Sesuai KTP)");
        jPanel1.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 410, -1, -1));
        jPanel1.add(txtAlamat, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 210, 400, 40));

        comboKeperluan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboKeperluanActionPerformed(evt);
            }
        });
        jPanel1.add(comboKeperluan, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 30, 400, -1));

        comboProvinsi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboProvinsiActionPerformed(evt);
            }
        });
        jPanel1.add(comboProvinsi, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 280, 400, 20));

        comboKota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboKotaActionPerformed(evt);
            }
        });
        jPanel1.add(comboKota, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 330, 400, -1));

        comboKec.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboKecActionPerformed(evt);
            }
        });
        jPanel1.add(comboKec, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 380, 400, -1));

        jPanel2.setBackground(new java.awt.Color(255, 204, 204));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel15.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel15.setText("* Pilih Kesatuan Wilayah dibawah ini sesuai dengan domisili KTP atau Identitas daerah pemohon");
        jPanel2.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 60, 400, 30));

        jPanel3.setBackground(new java.awt.Color(255, 204, 204));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel16.setFont(new java.awt.Font("Calibri", 0, 10)); // NOI18N
        jLabel16.setText("* Isi data alamat dibawah ini sesuai dengan domisili KTP atau Identitas daerah pemohon.");
        jPanel3.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 150, 400, 30));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 900, 480));

        btnBatal.setBackground(java.awt.Color.red);
        btnBatal.setText("Batalkan");
        btnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBatalActionPerformed(evt);
            }
        });
        getContentPane().add(btnBatal, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 610, -1, -1));

        btnKembali.setText("Kembali");
        btnKembali.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKembaliActionPerformed(evt);
            }
        });
        getContentPane().add(btnKembali, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 610, -1, -1));

        btnLanjut.setText("Lanjut");
        btnLanjut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLanjutActionPerformed(evt);
            }
        });
        getContentPane().add(btnLanjut, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 610, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnhubkelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnhubkelActionPerformed
        
    }//GEN-LAST:event_btnhubkelActionPerformed

    private void btnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBatalActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnBatalActionPerformed

    private void btnsatwilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsatwilActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnsatwilActionPerformed

    private void btnKembaliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnKembaliActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnKembaliActionPerformed

    private void btndataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndataActionPerformed
        
    }//GEN-LAST:event_btndataActionPerformed

    private void btnperpidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnperpidActionPerformed
        
    }//GEN-LAST:event_btnperpidActionPerformed

    private void btnpendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnpendActionPerformed
       
    }//GEN-LAST:event_btnpendActionPerformed

    private void btncirfisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncirfisActionPerformed
        
    }//GEN-LAST:event_btncirfisActionPerformed

    private void btnlampActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnlampActionPerformed
        
    }//GEN-LAST:event_btnlampActionPerformed

    private void btnketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnketActionPerformed
        
    }//GEN-LAST:event_btnketActionPerformed

    private void btnLanjutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLanjutActionPerformed
       this.dispose();
        pc.simpanForm(this);
       
    }//GEN-LAST:event_btnLanjutActionPerformed

    private void comboKeperluanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboKeperluanActionPerformed
        // TODO add your handling code here:
        tampilSatwil();
    }//GEN-LAST:event_comboKeperluanActionPerformed

    private void comboProvinsiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboProvinsiActionPerformed
        // TODO add your handling code here:
        tampilKota();
    }//GEN-LAST:event_comboProvinsiActionPerformed

    private void comboSatwilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboSatwilActionPerformed
        // TODO add your handling code here:
        tampilProvinsi();
    }//GEN-LAST:event_comboSatwilActionPerformed

    private void comboKecActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboKecActionPerformed
        // TODO add your handling code here:
        tampilKel();
    }//GEN-LAST:event_comboKecActionPerformed

    private void comboKotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboKotaActionPerformed
        // TODO add your handling code here:
        tampilKec();
    }//GEN-LAST:event_comboKotaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(skck_form_satwil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(skck_form_satwil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(skck_form_satwil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(skck_form_satwil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new skck_form_satwil().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBatal;
    private javax.swing.JButton btnKembali;
    private javax.swing.JButton btnLanjut;
    private javax.swing.JButton btncirfis;
    private javax.swing.JButton btndata;
    private javax.swing.JButton btnhubkel;
    private javax.swing.JButton btnket;
    private javax.swing.JButton btnlamp;
    private javax.swing.JButton btnpend;
    private javax.swing.JButton btnperpid;
    private javax.swing.JButton btnsatwil;
    private javax.swing.JComboBox<String> comboKec;
    private javax.swing.JComboBox<String> comboKel;
    private javax.swing.JComboBox<String> comboKeperluan;
    private javax.swing.JComboBox<String> comboKota;
    private javax.swing.JComboBox<String> comboProvinsi;
    private javax.swing.JComboBox<String> comboSatwil;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JTextField txtAlamat;
    // End of variables declaration//GEN-END:variables

    @Override
    public void onChange(satwilModel satwil) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
