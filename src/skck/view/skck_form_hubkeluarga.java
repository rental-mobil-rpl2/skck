/*
    kelompok 2 skck xii rpl 2
        anggota :
        1. Akmal Fauzi Salman
        2. Dina 
        3. Fajar Ramadhan
        4. Gan gan Ahmad 
        5. Rafli Santana
        6. Zhahra Sahira
        
*/


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.view;
import com.toedter.calendar.JDateChooser;
import java.io.File;
import skck.controller.controllerHubkeluarga;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.*;
import skck.controller.controllerDatadiri;
import skck.koneksi.koneksi;

/**
 *
 * @author gu
 */
public class skck_form_hubkeluarga extends javax.swing.JFrame {
controllerHubkeluarga cbt;
ButtonGroup buttonGroup1,buttonGroup2,buttonGroup3;

    /**
     * Creates new form skck_form_daftar_satwil
     */
    public skck_form_hubkeluarga() {
        initComponents();
        cbt = new controllerHubkeluarga(this);

        radio1.setActionCommand("WNA");
        radio2.setActionCommand("WNI");
        radio3.setActionCommand("WNA");
        radio4.setActionCommand("WNI");
        radio5.setActionCommand("WNA");
        radio6.setActionCommand("WNI");
        
        buttonGroup1=new ButtonGroup();
        buttonGroup1.add(radio1);
        buttonGroup1.add(radio2);
        buttonGroup2=new ButtonGroup();
        buttonGroup2.add(radio3);
        buttonGroup2.add(radio4);
        buttonGroup3=new ButtonGroup();
        buttonGroup3.add(radio5);
        buttonGroup3.add(radio6);
        
        label1.setVisible(false);
        label2.setVisible(false);
        label3.setVisible(false);
        
        tampil_agama();
        tampil_propinsi();
        tampil_kabkota();
        tampil_kecamatan();
        tampil_kelurahan();

    }
    public void tampil_agama()
    {
        try {
        Connection con = koneksi.connection();
        Statement stt = con.createStatement();
        String sql = "select agama from agama order by id_agama asc";      // disini saya menampilkan NIM, anda dapat menampilkan
        ResultSet res = stt.executeQuery(sql);                                // yang anda ingin kan
        
        while(res.next()){
            Object[] ob = new Object[3];
            ob[0] = res.getString(1);
            
            comboAgama1.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
            comboAgama2.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
            comboAgama3.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
        }
        res.close(); stt.close();
         
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void tampil_propinsi()
    {
        try {
        Connection con = koneksi.connection();
        Statement stt = con.createStatement();
        String sql = "select nama_propinsi from m_ipropinsi order by id_propinsi asc";      // disini saya menampilkan NIM, anda dapat menampilkan
        ResultSet res = stt.executeQuery(sql);                                // yang anda ingin kan
        
        while(res.next()){
            Object[] ob = new Object[3];
            ob[0] = res.getString(1);
            
            comboPropinsi1.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
            comboPropinsi2.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
            comboPropinsi3.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
        }
        res.close(); stt.close();
         
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void tampil_kabkota()
    {
        try {
        Connection con = koneksi.connection();
        Statement stt = con.createStatement();
        String sql = "select nama_kabkota from m_ikabkota order by id_kabkota asc";      // disini saya menampilkan NIM, anda dapat menampilkan
        ResultSet res = stt.executeQuery(sql);                                // yang anda ingin kan
        
        while(res.next()){
            Object[] ob = new Object[3];
            ob[0] = res.getString(1);
            
            comboKabkota1.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
            comboKabkota2.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
            comboKabkota3.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
        }
        res.close(); stt.close();
         
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void tampil_kecamatan()
    {
        try {
        Connection con = koneksi.connection();
        Statement stt = con.createStatement();
        String sql = "select nama_kecamatan from m_ikecamatan order by id_kecamatan asc";      // disini saya menampilkan NIM, anda dapat menampilkan
        ResultSet res = stt.executeQuery(sql);                                // yang anda ingin kan
        
        while(res.next()){
            Object[] ob = new Object[3];
            ob[0] = res.getString(1);
            
            comboKecamatan1.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
            comboKecamatan2.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
            comboKecamatan3.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
        }
        res.close(); stt.close();
         
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void tampil_kelurahan()
    {
        try {
        Connection con = koneksi.connection();
        Statement stt = con.createStatement();
        String sql = "select nama_kelurahan from m_ikelurahan order by id_kelurahan asc";      // disini saya menampilkan NIM, anda dapat menampilkan
        ResultSet res = stt.executeQuery(sql);                                // yang anda ingin kan
        
        while(res.next()){
            Object[] ob = new Object[3];
            ob[0] = res.getString(1);
            
            comboKelurahan1.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
            comboKelurahan2.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
            comboKelurahan3.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
        }
        res.close(); stt.close();
         
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bg1 = new javax.swing.ButtonGroup();
        bg2 = new javax.swing.ButtonGroup();
        bg3 = new javax.swing.ButtonGroup();
        jProgressBar1 = new javax.swing.JProgressBar();
        btnsatwil = new javax.swing.JButton();
        btndata = new javax.swing.JButton();
        btnhubkel = new javax.swing.JButton();
        btnpend = new javax.swing.JButton();
        btnperpid = new javax.swing.JButton();
        btncirfis = new javax.swing.JButton();
        btnlamp = new javax.swing.JButton();
        btnket = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        btnLanjut = new javax.swing.JButton();
        btnBatal = new javax.swing.JButton();
        btnKembali = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtPekerjaan1 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtNama1 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        comboHub = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        radio2 = new javax.swing.JRadioButton();
        radio1 = new javax.swing.JRadioButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtAlamat1 = new javax.swing.JTextArea();
        jLabel11 = new javax.swing.JLabel();
        comboAgama1 = new javax.swing.JComboBox<>();
        jLabel12 = new javax.swing.JLabel();
        comboKelurahan1 = new javax.swing.JComboBox<>();
        comboPropinsi1 = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        comboKabkota1 = new javax.swing.JComboBox<>();
        jLabel14 = new javax.swing.JLabel();
        comboKecamatan1 = new javax.swing.JComboBox<>();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txtPekerjaan2 = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txtNama2 = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        radio4 = new javax.swing.JRadioButton();
        radio3 = new javax.swing.JRadioButton();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtAlamat2 = new javax.swing.JTextArea();
        jLabel23 = new javax.swing.JLabel();
        comboAgama2 = new javax.swing.JComboBox<>();
        jLabel24 = new javax.swing.JLabel();
        comboKelurahan2 = new javax.swing.JComboBox<>();
        comboPropinsi2 = new javax.swing.JComboBox<>();
        jLabel25 = new javax.swing.JLabel();
        comboKabkota2 = new javax.swing.JComboBox<>();
        jLabel26 = new javax.swing.JLabel();
        comboKecamatan2 = new javax.swing.JComboBox<>();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        txtPekerjaan3 = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        txtNama3 = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        radio6 = new javax.swing.JRadioButton();
        radio5 = new javax.swing.JRadioButton();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtAlamat3 = new javax.swing.JTextArea();
        jLabel35 = new javax.swing.JLabel();
        comboAgama3 = new javax.swing.JComboBox<>();
        jLabel36 = new javax.swing.JLabel();
        comboKelurahan3 = new javax.swing.JComboBox<>();
        comboPropinsi3 = new javax.swing.JComboBox<>();
        jLabel37 = new javax.swing.JLabel();
        comboKabkota3 = new javax.swing.JComboBox<>();
        jLabel38 = new javax.swing.JLabel();
        comboKecamatan3 = new javax.swing.JComboBox<>();
        label2 = new javax.swing.JLabel();
        label1 = new javax.swing.JLabel();
        label3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jProgressBar1.setValue(38);
        getContentPane().add(jProgressBar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 880, 20));

        btnsatwil.setText("Satwil");
        btnsatwil.setEnabled(false);
        btnsatwil.setFocusPainted(false);
        btnsatwil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsatwilActionPerformed(evt);
            }
        });
        getContentPane().add(btnsatwil, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 110, -1));

        btndata.setText("Data Pribadi");
        btndata.setEnabled(false);
        btndata.setFocusPainted(false);
        btndata.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndataActionPerformed(evt);
            }
        });
        getContentPane().add(btndata, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 40, 110, -1));

        btnhubkel.setText("Hub. Keluarga");
        btnhubkel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnhubkelActionPerformed(evt);
            }
        });
        getContentPane().add(btnhubkel, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 40, 110, -1));

        btnpend.setText("Pendidikan");
        btnpend.setEnabled(false);
        btnpend.setFocusPainted(false);
        btnpend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnpendActionPerformed(evt);
            }
        });
        getContentPane().add(btnpend, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 40, 110, -1));

        btnperpid.setText("Perkara Pidana");
        btnperpid.setEnabled(false);
        btnperpid.setFocusPainted(false);
        btnperpid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnperpidActionPerformed(evt);
            }
        });
        getContentPane().add(btnperpid, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 40, 110, -1));

        btncirfis.setText("Ciri Fisik");
        btncirfis.setEnabled(false);
        btncirfis.setFocusPainted(false);
        btncirfis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncirfisActionPerformed(evt);
            }
        });
        getContentPane().add(btncirfis, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 40, 110, -1));

        btnlamp.setText("Lampiran");
        btnlamp.setEnabled(false);
        btnlamp.setFocusPainted(false);
        btnlamp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnlampActionPerformed(evt);
            }
        });
        getContentPane().add(btnlamp, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 40, 110, -1));

        btnket.setText("Keterangan");
        btnket.setEnabled(false);
        btnket.setFocusPainted(false);
        btnket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnketActionPerformed(evt);
            }
        });
        getContentPane().add(btnket, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 40, 110, -1));

        jLabel2.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel2.setText("Form. Pendaftaran");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        btnLanjut.setText("Lanjut");
        btnLanjut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLanjutActionPerformed(evt);
            }
        });
        getContentPane().add(btnLanjut, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 620, -1, -1));

        btnBatal.setBackground(java.awt.Color.red);
        btnBatal.setText("Batalkan");
        btnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBatalActionPerformed(evt);
            }
        });
        getContentPane().add(btnBatal, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 620, -1, -1));

        btnKembali.setText("Kembali");
        btnKembali.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKembaliActionPerformed(evt);
            }
        });
        getContentPane().add(btnKembali, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 620, -1, -1));

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel3.setFont(new java.awt.Font("Calibri", 2, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(153, 153, 153));
        jLabel3.setText("Menerangkan hal-hal sebagai jawaban/keterangan atas pertanyaan-pertanyaan diajukan sebagai berikut:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        jLabel4.setFont(new java.awt.Font("Calibri", 2, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(153, 153, 153));
        jLabel4.setText("A. Data Istri / Suami");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, -1, -1));

        jLabel5.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel5.setText("Pekerjaan");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 140, -1, -1));
        jPanel1.add(txtPekerjaan1, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 160, 470, -1));

        jLabel6.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel6.setText("Nama Lengkap");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 80, -1, -1));
        jPanel1.add(txtNama1, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 100, 510, -1));

        jLabel7.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel7.setText("Hubungan");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 80, -1, -1));

        comboHub.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ISTRI", "SUAMI" }));
        jPanel1.add(comboHub, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 100, 90, -1));

        jLabel8.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel8.setText("Kelurahan");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 260, -1, -1));

        bg1.add(radio2);
        radio2.setText("WNI");
        jPanel1.add(radio2, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 160, -1, -1));

        bg1.add(radio1);
        radio1.setText("WNA");
        jPanel1.add(radio1, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 160, -1, -1));

        jLabel9.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel9.setText("Kewarganegaraan");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 140, -1, -1));

        jLabel10.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel10.setText("Agama");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 140, -1, -1));

        txtAlamat1.setColumns(20);
        txtAlamat1.setRows(5);
        jScrollPane1.setViewportView(txtAlamat1);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 220, 270, 80));

        jLabel11.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel11.setText("Alamat (Saat Ini)");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 200, -1, -1));

        jPanel1.add(comboAgama1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 160, 170, -1));

        jLabel12.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel12.setText("Provinsi");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 200, -1, -1));

        jPanel1.add(comboKelurahan1, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 280, 250, -1));

        jPanel1.add(comboPropinsi1, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 220, 250, -1));

        jLabel13.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel13.setText("Kabupaten / Kota");
        jPanel1.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 200, -1, -1));

        jPanel1.add(comboKabkota1, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 220, 250, -1));

        jLabel14.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel14.setText("Kecamatan");
        jPanel1.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 260, -1, -1));

        jPanel1.add(comboKecamatan1, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 280, 250, -1));

        jLabel16.setFont(new java.awt.Font("Calibri", 2, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(153, 153, 153));
        jLabel16.setText("B. Data Ayah");
        jPanel1.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 330, -1, -1));

        jLabel17.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel17.setText("Pekerjaan");
        jPanel1.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 420, -1, -1));
        jPanel1.add(txtPekerjaan2, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 440, 470, -1));

        jLabel18.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel18.setText("Nama Lengkap");
        jPanel1.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 360, -1, -1));
        jPanel1.add(txtNama2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 380, 650, -1));

        jLabel20.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel20.setText("Kelurahan");
        jPanel1.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 540, -1, -1));

        bg2.add(radio4);
        radio4.setText("WNI");
        jPanel1.add(radio4, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 440, -1, -1));

        bg2.add(radio3);
        radio3.setText("WNA");
        jPanel1.add(radio3, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 440, -1, -1));

        jLabel21.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel21.setText("Kewarganegaraan");
        jPanel1.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 420, -1, -1));

        jLabel22.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel22.setText("Agama");
        jPanel1.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 420, -1, -1));

        txtAlamat2.setColumns(20);
        txtAlamat2.setRows(5);
        jScrollPane2.setViewportView(txtAlamat2);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 500, 270, 80));

        jLabel23.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel23.setText("Alamat (Saat Ini)");
        jPanel1.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 480, -1, -1));

        jPanel1.add(comboAgama2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 440, 170, -1));

        jLabel24.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel24.setText("Provinsi");
        jPanel1.add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 480, -1, -1));

        jPanel1.add(comboKelurahan2, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 560, 250, -1));

        jPanel1.add(comboPropinsi2, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 500, 250, -1));

        jLabel25.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel25.setText("Kabupaten / Kota");
        jPanel1.add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 480, -1, -1));

        jPanel1.add(comboKabkota2, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 500, 250, -1));

        jLabel26.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel26.setText("Kecamatan");
        jPanel1.add(jLabel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 540, -1, -1));

        jPanel1.add(comboKecamatan2, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 560, 250, -1));

        jLabel28.setFont(new java.awt.Font("Calibri", 2, 14)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(153, 153, 153));
        jLabel28.setText("C. Data Ibu");
        jPanel1.add(jLabel28, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 610, -1, -1));

        jLabel29.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel29.setText("Pekerjaan");
        jPanel1.add(jLabel29, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 700, -1, -1));
        jPanel1.add(txtPekerjaan3, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 720, 470, -1));

        jLabel30.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel30.setText("Nama Lengkap");
        jPanel1.add(jLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 640, -1, -1));
        jPanel1.add(txtNama3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 660, 650, -1));

        jLabel32.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel32.setText("Kelurahan");
        jPanel1.add(jLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 820, -1, -1));

        bg3.add(radio6);
        radio6.setText("WNI");
        jPanel1.add(radio6, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 720, -1, -1));

        bg3.add(radio5);
        radio5.setText("WNA");
        jPanel1.add(radio5, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 720, -1, -1));

        jLabel33.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel33.setText("Kewarganegaraan");
        jPanel1.add(jLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 700, -1, -1));

        jLabel34.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel34.setText("Agama");
        jPanel1.add(jLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 700, -1, -1));

        txtAlamat3.setColumns(20);
        txtAlamat3.setRows(5);
        jScrollPane3.setViewportView(txtAlamat3);

        jPanel1.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 780, 270, 80));

        jLabel35.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel35.setText("Alamat (Saat Ini)");
        jPanel1.add(jLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 760, -1, -1));

        jPanel1.add(comboAgama3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 720, 170, -1));

        jLabel36.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel36.setText("Provinsi");
        jPanel1.add(jLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 760, -1, -1));

        jPanel1.add(comboKelurahan3, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 840, 250, -1));

        jPanel1.add(comboPropinsi3, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 780, 250, -1));

        jLabel37.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel37.setText("Kabupaten / Kota");
        jPanel1.add(jLabel37, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 760, -1, -1));

        jPanel1.add(comboKabkota3, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 780, 250, -1));

        jLabel38.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel38.setText("Kecamatan");
        jPanel1.add(jLabel38, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 820, -1, -1));

        jPanel1.add(comboKecamatan3, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 840, 250, -1));
        jPanel1.add(label2, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 750, 50, 20));
        jPanel1.add(label1, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 190, 50, 20));
        jPanel1.add(label3, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 750, 50, 20));

        jScrollPane5.setViewportView(jPanel1);

        getContentPane().add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, -1, 490));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnhubkelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnhubkelActionPerformed
        
    }//GEN-LAST:event_btnhubkelActionPerformed

    private void btnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBatalActionPerformed
      this.dispose();
    }//GEN-LAST:event_btnBatalActionPerformed

    private void btnsatwilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsatwilActionPerformed
      
    }//GEN-LAST:event_btnsatwilActionPerformed

    private void btnKembaliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnKembaliActionPerformed
        this.dispose();
        skck_form_data_diri obj = new skck_form_data_diri();
        obj.setVisible(true);
    }//GEN-LAST:event_btnKembaliActionPerformed

    private void btndataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndataActionPerformed
       
    }//GEN-LAST:event_btndataActionPerformed

    private void btnperpidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnperpidActionPerformed
        
    }//GEN-LAST:event_btnperpidActionPerformed

    private void btnpendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnpendActionPerformed
        
    }//GEN-LAST:event_btnpendActionPerformed

    private void btncirfisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncirfisActionPerformed
      
    }//GEN-LAST:event_btncirfisActionPerformed

    private void btnlampActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnlampActionPerformed
        
    }//GEN-LAST:event_btnlampActionPerformed

    private void btnketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnketActionPerformed
        
    }//GEN-LAST:event_btnketActionPerformed

    private void btnLanjutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLanjutActionPerformed
        label1.setText(buttonGroup1.getSelection().getActionCommand());
        label2.setText(buttonGroup2.getSelection().getActionCommand());
        label3.setText(buttonGroup3.getSelection().getActionCommand());
        cbt.insert();

        this.dispose();
        skck_form_perkara_pidana obj = new skck_form_perkara_pidana();
        obj.setVisible(true);
    }//GEN-LAST:event_btnLanjutActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(skck_form_hubkeluarga.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(skck_form_hubkeluarga.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(skck_form_hubkeluarga.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(skck_form_hubkeluarga.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new skck_form_hubkeluarga().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bg1;
    private javax.swing.ButtonGroup bg2;
    private javax.swing.ButtonGroup bg3;
    private javax.swing.JButton btnBatal;
    private javax.swing.JButton btnKembali;
    private javax.swing.JButton btnLanjut;
    private javax.swing.JButton btncirfis;
    private javax.swing.JButton btndata;
    private javax.swing.JButton btnhubkel;
    private javax.swing.JButton btnket;
    private javax.swing.JButton btnlamp;
    private javax.swing.JButton btnpend;
    private javax.swing.JButton btnperpid;
    private javax.swing.JButton btnsatwil;
    private javax.swing.JComboBox<String> comboAgama1;
    private javax.swing.JComboBox<String> comboAgama2;
    private javax.swing.JComboBox<String> comboAgama3;
    private javax.swing.JComboBox<String> comboHub;
    private javax.swing.JComboBox<String> comboKabkota1;
    private javax.swing.JComboBox<String> comboKabkota2;
    private javax.swing.JComboBox<String> comboKabkota3;
    private javax.swing.JComboBox<String> comboKecamatan1;
    private javax.swing.JComboBox<String> comboKecamatan2;
    private javax.swing.JComboBox<String> comboKecamatan3;
    private javax.swing.JComboBox<String> comboKelurahan1;
    private javax.swing.JComboBox<String> comboKelurahan2;
    private javax.swing.JComboBox<String> comboKelurahan3;
    private javax.swing.JComboBox<String> comboPropinsi1;
    private javax.swing.JComboBox<String> comboPropinsi2;
    private javax.swing.JComboBox<String> comboPropinsi3;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JLabel label1;
    private javax.swing.JLabel label2;
    private javax.swing.JLabel label3;
    private javax.swing.JRadioButton radio1;
    private javax.swing.JRadioButton radio2;
    private javax.swing.JRadioButton radio3;
    private javax.swing.JRadioButton radio4;
    private javax.swing.JRadioButton radio5;
    private javax.swing.JRadioButton radio6;
    private javax.swing.JTextArea txtAlamat1;
    private javax.swing.JTextArea txtAlamat2;
    private javax.swing.JTextArea txtAlamat3;
    private javax.swing.JTextField txtNama1;
    private javax.swing.JTextField txtNama2;
    private javax.swing.JTextField txtNama3;
    private javax.swing.JTextField txtPekerjaan1;
    private javax.swing.JTextField txtPekerjaan2;
    private javax.swing.JTextField txtPekerjaan3;
    // End of variables declaration//GEN-END:variables
    public JComboBox<String> getTxtHub(){
        return comboHub;
    }
    public JTextField getTxtNama1(){
        return txtNama1;
    }
    public JComboBox<String> getTxtAgama1(){
        return comboAgama1;
    }
    public JLabel getTxtKewarganegaraan1(){
        return label1;
    }
    public JTextField getTxtPekerjaan1(){
        return txtPekerjaan1;
    }
    public JTextArea getTxtAlamat1(){
        return txtAlamat1;
    }
    public JComboBox<String> getTxtPropinsi1(){
        return comboPropinsi1;
    }
    public JComboBox<String> getTxtKabkota1(){
        return comboKabkota1;
    }
    public JComboBox<String> getTxtKecamatan1(){
        return comboKecamatan1;
    }
    public JComboBox<String> getTxtKelurahan1(){
        return comboKelurahan1;
    }
    public JTextField getTxtNama2(){
        return txtNama2;
    }
    public JComboBox<String> getTxtAgama2(){
        return comboAgama2;
    }
    public JLabel getTxtKewarganegaraan2(){
        return label1;
    }
    public JTextField getTxtPekerjaan2(){
        return txtPekerjaan2;
    }
    public JTextArea getTxtAlamat2(){
        return txtAlamat2;
    }
    public JComboBox<String> getTxtPropinsi2(){
        return comboPropinsi2;
    }
    public JComboBox<String> getTxtKabkota2(){
        return comboKabkota2;
    }
    public JComboBox<String> getTxtKecamatan2(){
        return comboKecamatan2;
    }
    public JComboBox<String> getTxtKelurahan2(){
        return comboKelurahan2;
    }
    public JTextField getTxtNama3(){
        return txtNama3;
    }
    public JComboBox<String> getTxtAgama3(){
        return comboAgama3;
    }
    public JLabel getTxtKewarganegaraan3(){
        return label2;
    }
    public JTextField getTxtPekerjaan3(){
        return txtPekerjaan3;
    }
    public JTextArea getTxtAlamat3(){
        return txtAlamat3;
    }
    public JComboBox<String> getTxtPropinsi3(){
        return comboPropinsi3;
    }
    public JComboBox<String> getTxtKabkota3(){
        return comboKabkota3;
    }
    public JComboBox<String> getTxtKecamatan3(){
        return comboKecamatan3;
    }
    public JComboBox<String> getTxtKelurahan3(){
        return comboKelurahan3;
    }
}
