/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.DAO;

import skck.koneksi.koneksi;
import skck.model.pendidikan;
import skck.DAOImplement.implementPendidikan;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
/**
 *
 * @author Fajar
 */
public class daoPendidikan implements implementPendidikan {
    
    Connection connection;
    final String insert = "INSERT INTO pendidikan (tingkat, univ,provinsi, kabkota, tahun) VALUES (?, ?, ?,?, ?);";
    final String update = "UPDATE pendidikan set tingkat=?, univ=?, provinsi=?, kabkota=?, tahun=? where id_pend=? ;";
    final String delete = "DELETE FROM pendidikan where id_pend=? ;";
    final String select = "SELECT * FROM pendidikan;";
    
    public daoPendidikan() {
        connection = koneksi.connection();
    }

    public void insert(pendidikan b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, b.getTingkat());
            statement.setString(2, b.getUniv());
            statement.setString(3, b.getProvinsi());
            statement.setString(4, b.getKabkota());
            statement.setInt(5, b.getTahun());
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            while (rs.next()) {
                b.setId_pend(rs.getInt(1));
            }
           
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void update(pendidikan b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(update);
            statement.setString(1, b.getTingkat());
            statement.setString(2, b.getUniv());
            statement.setString(3, b.getProvinsi());
            statement.setString(4, b.getKabkota());
            statement.setInt(5, b.getTahun());
            statement.setInt(6, b.getId_pend());
            statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void delete(int id_pend) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(delete);

            statement.setInt(1, id_pend);
            statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public List<pendidikan> getALL() {
        List<pendidikan> lb = null;
        try {
            lb = new ArrayList<pendidikan>();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                pendidikan b = new pendidikan();
                b.setId_pend(rs.getInt("id_pend"));
                b.setTingkat(rs.getString("tingkat"));
                b.setUniv(rs.getString("univ"));
                b.setProvinsi(rs.getString("provinsi"));
                b.setKabkota(rs.getString("kabkota"));
                b.setTahun(rs.getInt("tahun"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoPerkarapidana.class.getName()).log(Level.SEVERE, null, ex);
        }

        return lb;
    }
    
}
