/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.DAO;
import skck.koneksi.koneksi;
import skck.model.hubkeluarga;
import skck.DAOImplement.implementHubkeluarga;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
/**
 *
 * @author Akmal
 */
public class daoHubkeluarga implements implementHubkeluarga{
    
    Connection connection;
    final String insert = "INSERT INTO hub_keluarga (hub,nama1,agama1,kewarganegaraan1,pekerjaan1,alamat1,propinsi1,kabkota1,kecamatan1,kelurahan1,nama2,agama2,kewarganegaraan2,pekerjaan2,alamat2,propinsi2,kabkota2,kecamatan2,kelurahan2,nama3,agama3,kewarganegaraan3,pekerjaan3,alamat3,propinsi3,kabkota3,kecamatan3,kelurahan3) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
    final String update = "UPDATE hub_keluarga set hub=?, nama1=?,kewarganegaraan1=?,agama1=?,pekerjaan1=?,alamat1=?,propinsi1=?,kabkota1=?,kecamatan1=?,kelurahan1=?,nama2=?,kewarganegaraan2=?,agama2=?,pekerjaan2=?,alamat2=?,propinsi2=?,kabkota2=?,kecamatan2=?,kelurahan2=?,nama3=?,kewarganegaraan3=?,agama3=?,pekerjaan3=?,alamat3=?,propinsi3=?,kabkota3=?,kecamatan3=?,kelurahan3=?, where id_hub=? ;";
    final String delete = "DELETE FROM hub_keluarga where id_hub=? ;";
    final String select = "SELECT * FROM hub_keluarga;";
    
    public daoHubkeluarga() {
        connection = koneksi.connection();
    }
    
    public void insert(hubkeluarga b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, b.getHub());
            statement.setString(2, b.getNama1());
            statement.setString(3, b.getKewarganegaraan1());
            statement.setString(4, b.getAgama1());
            statement.setString(5, b.getPekerjaan1());
            statement.setString(6, b.getAlamat1());
            statement.setString(7, b.getPropinsi1());
            statement.setString(8, b.getKabkota1());
            statement.setString(9, b.getKecamatan1());
            statement.setString(10, b.getKelurahan1());
            statement.setString(11, b.getNama2());
            statement.setString(12, b.getKewarganegaraan2());
            statement.setString(13, b.getAgama2());
            statement.setString(14, b.getPekerjaan2());
            statement.setString(15, b.getAlamat2());
            statement.setString(16, b.getPropinsi2());
            statement.setString(17, b.getKabkota2());
            statement.setString(18, b.getKecamatan2());
            statement.setString(19, b.getKelurahan2());
            statement.setString(20, b.getNama3());
            statement.setString(21, b.getKewarganegaraan3());
            statement.setString(22, b.getAgama3());
            statement.setString(23, b.getPekerjaan3());
            statement.setString(24, b.getAlamat3());
            statement.setString(25, b.getPropinsi3());
            statement.setString(26, b.getKabkota3());
            statement.setString(27, b.getKecamatan3());
            statement.setString(28, b.getKelurahan3());
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            while (rs.next()) {
                b.setId_hub(rs.getInt(1));
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }


    public List<hubkeluarga> getALL() {
        List<hubkeluarga> lb = null;
        try {
            lb = new ArrayList<hubkeluarga>();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                hubkeluarga b = new hubkeluarga();
                b.setId_hub(rs.getInt("id_hub"));
                b.setHub(rs.getString("hub"));
                b.setNama1(rs.getString("nama1"));
                b.setKewarganegaraan1(rs.getString("kewarganegaraan1"));
                b.setAgama1(rs.getString("agama1"));
                b.setPekerjaan1(rs.getString("pekerjaan1"));
                b.setAlamat1(rs.getString("alamat1"));
                b.setPropinsi1(rs.getString("propinsi1"));
                b.setKabkota1(rs.getString("kabkota1"));
                b.setKecamatan1(rs.getString("kecamatan1"));
                b.setKelurahan1(rs.getString("kelurahan1"));
                b.setNama2(rs.getString("nama2"));
                b.setKewarganegaraan2(rs.getString("kewarganegaraan2"));
                b.setAgama2(rs.getString("agama2"));
                b.setPekerjaan2(rs.getString("pekerjaan2"));
                b.setAlamat2(rs.getString("alamat2"));
                b.setPropinsi2(rs.getString("propinsi2"));
                b.setKabkota2(rs.getString("kabkota2"));
                b.setKecamatan2(rs.getString("kecamatan2"));
                b.setKelurahan2(rs.getString("kelurahan2"));
                b.setNama3(rs.getString("nama3"));
                b.setKewarganegaraan3(rs.getString("kewarganegaraan3"));
                b.setAgama3(rs.getString("agama3"));
                b.setPekerjaan3(rs.getString("pekerjaan3"));
                b.setAlamat3(rs.getString("alamat3"));
                b.setPropinsi3(rs.getString("propinsi3"));
                b.setKabkota3(rs.getString("kabkota3"));
                b.setKecamatan3(rs.getString("kecamatan3"));
                b.setKelurahan3(rs.getString("kelurahan3"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoHubkeluarga.class.getName()).log(Level.SEVERE, null, ex);
        }

        return lb;
    }

//    public List<mahasiswa> getCariNama(String nama) {
//        List<mahasiswa> lb = null;
//        try {
//            lb = new ArrayList<mahasiswa>();
//            PreparedStatement st = connection.prepareStatement(carinama);
//            st.setString(1, "%" + nama + "%");
//            ResultSet rs = st.executeQuery();
//            while (rs.next()) {
//                mahasiswa b = new mahasiswa();
//                b.setId(rs.getInt("id"));
//                b.setNim(rs.getString("nim"));
//                b.setNama(rs.getString("nama"));
//                b.setJk(rs.getString("jk"));
//                b.setAlamat(rs.getString("alamat"));
//                lb.add(b);
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(daoMahasiswa.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return lb;
//    }

}
