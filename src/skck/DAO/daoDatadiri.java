/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.DAO;
import skck.koneksi.koneksi;
import skck.model.datadiri;
import skck.DAOImplement.implementDatadiri;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
/**
 *
 * @author Akmal
 */
public class daoDatadiri implements implementDatadiri{
    
    Connection connection;
    final String insert = "INSERT INTO data_pribadi (nama,tempat_lahir, tanggal_lahir,jenis_kelamin,status_perkawinan,kewarganegaraan,agama,pekerjaan,no_telp,alamat,propinsi,kabkota,kecamatan,kelurahan,type_identitas,no_identitas,no_paspor,foto) VALUES (?,?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
    final String update = "UPDATE data_pribadi set nama=?,tempat_lahir=?, tanggal_lahir=?,jenis_kelamin=?,status_perkawinan=?,kewarganegaraan=?,agama=?,pekerjaan=?,no_telp=?,alamat=?,propinsi=?,kabkota=?,kecamatan=?,kelurahan=?,type_identitas=?,no_identitas=?,no_paspor=?,foto=? where id_mobil=? ;";
    final String delete = "DELETE FROM data_pribadi where id_data=? ;";
    final String select = "SELECT * FROM data_pribadi;";
    
    public daoDatadiri() {
        connection = koneksi.connection();
    }
    
    public void insert(datadiri b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, b.getNama());
            statement.setString(2, b.getTempat_lahir());
            statement.setString(3, b.getTanggal_lahir());
            statement.setString(4, b.getJenis_kelamin());
            statement.setString(5, b.getStatus_perkawinan());
            statement.setString(6, b.getKewarganegaraan());
            statement.setString(7, b.getAgama());
            statement.setString(8, b.getPekerjaan());
            statement.setString(9, b.getNo_telp());
            statement.setString(10, b.getAlamat());
            statement.setString(11, b.getPropinsi());
            statement.setString(12, b.getKabkota());
            statement.setString(13, b.getKecamatan());
            statement.setString(14, b.getKelurahan());
            statement.setString(15, b.getType_identitas());
            statement.setString(16, b.getNo_identitas());
            statement.setString(17, b.getNo_paspor());
            statement.setString(18, b.getFoto());
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            while (rs.next()) {
                b.setId_data(rs.getInt(1));
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }


    public List<datadiri> getALL() {
        List<datadiri> lb = null;
        try {
            lb = new ArrayList<datadiri>();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                datadiri b = new datadiri();
                b.setId_data(rs.getInt("id_data"));
                b.setNama(rs.getString("nama"));
                b.setTempat_lahir(rs.getString("tempat_lahir"));
                b.setTanggal_lahir(rs.getString("tanggal_lahir"));
                b.setJenis_kelamin(rs.getString("jenis_kelamin"));
                b.setStatus_perkawinan(rs.getString("status_perkawinan"));
                b.setKewarganegaraan(rs.getString("kewarganegaraan"));
                b.setAgama(rs.getString("agama"));
                b.setPekerjaan(rs.getString("pekerjaan"));
                b.setNo_telp(rs.getString("no_telp"));
                b.setAlamat(rs.getString("alamat"));
                b.setPropinsi(rs.getString("propinsi"));
                b.setKabkota(rs.getString("kabkota"));
                b.setKecamatan(rs.getString("kecamatan"));
                b.setKelurahan(rs.getString("kelurahan"));
                b.setType_identitas(rs.getString("type_identitas"));
                b.setNo_identitas(rs.getString("no_identitas"));
                b.setNo_paspor(rs.getString("no_paspor"));
                b.setFoto(rs.getString("foto"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoDatadiri.class.getName()).log(Level.SEVERE, null, ex);
        }

        return lb;
    }

//    public List<mahasiswa> getCariNama(String nama) {
//        List<mahasiswa> lb = null;
//        try {
//            lb = new ArrayList<mahasiswa>();
//            PreparedStatement st = connection.prepareStatement(carinama);
//            st.setString(1, "%" + nama + "%");
//            ResultSet rs = st.executeQuery();
//            while (rs.next()) {
//                mahasiswa b = new mahasiswa();
//                b.setId(rs.getInt("id"));
//                b.setNim(rs.getString("nim"));
//                b.setNama(rs.getString("nama"));
//                b.setJk(rs.getString("jk"));
//                b.setAlamat(rs.getString("alamat"));
//                lb.add(b);
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(daoMahasiswa.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return lb;
//    }

}
