/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.DAO;

import skck.koneksi.koneksi;
import skck.model.perkara_pidana;
import skck.DAOImplement.implementPerkarapidana;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
/**
 *
 * @author Fajar
 */
public class daoPerkarapidana implements implementPerkarapidana {
    
    Connection connection;
    final String insert = "INSERT INTO perkara_pidana (perkara, putusan_vonis,prosesper_kasus, sejauh_mn_proses, hukuman_norma_sosial, proses_sejauh_mn) VALUES (?, ?, ?,?, ?,?);";
    final String update = "UPDATE perkara_pidana set perkara=?, putusan_vonis=?, prosesper_kasus=?, sejauh_mn_proses=?, hukuman_norma_sosial=?, proses_sejauh_mn=? where id_per=? ;";
    final String delete = "DELETE FROM perkara_pidana where id_per=? ;";
    final String select = "SELECT * FROM perkara_pidana;";
    
    public daoPerkarapidana() {
        connection = koneksi.connection();
    }

    public void insert(perkara_pidana b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, b.getPerkara());
            statement.setString(2, b.getPutusan_vonis());
            statement.setString(3, b.getProsesper_kasus());
            statement.setString(4, b.getSejauh_mn_proses());
            statement.setString(5, b.getHukuman_norma_sosial());
            statement.setString(6, b.getProses_sejauh_mn());
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            while (rs.next()) {
                b.setId_per(rs.getInt(1));
            }
           
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void update(perkara_pidana b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(update);
            statement.setString(1, b.getPerkara());
            statement.setString(2, b.getPutusan_vonis());
            statement.setString(3, b.getProsesper_kasus());
            statement.setString(4, b.getSejauh_mn_proses());
            statement.setString(5, b.getHukuman_norma_sosial());
            statement.setString(6, b.getProses_sejauh_mn());
            statement.setInt(7, b.getId_per());
            statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void delete(int id_per) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(delete);

            statement.setInt(1, id_per);
            statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public List<perkara_pidana> getALL() {
        List<perkara_pidana> lb = null;
        try {
            lb = new ArrayList<perkara_pidana>();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                perkara_pidana b = new perkara_pidana();
                b.setId_per(rs.getInt("id_per"));
                b.setPerkara(rs.getString("perkara"));
                b.setPutusan_vonis(rs.getString("putusan_vonis"));
                b.setProsesper_kasus(rs.getString("prosesper_kasus"));
                b.setSejauh_mn_proses(rs.getString("sejauh_mn_proses"));
                b.setHukuman_norma_sosial(rs.getString("hukuman_norma_sosial"));
                b.setProses_sejauh_mn(rs.getString("proses_sejauh_mn"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoPerkarapidana.class.getName()).log(Level.SEVERE, null, ex);
        }

        return lb;
    }
    
}
