/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.controller;

import skck.DAO.daoPerkarapidana;
import skck.DAOImplement.implementPerkarapidana;
import skck.model.perkara_pidana;
import skck.view.skck_form_perkara_pidana;
import java.util.List;
import javax.swing.JOptionPane;
/**
 *
 * @author Fajar
 */
public class controllerPerkarapidana {
    skck_form_perkara_pidana frame;
    implementPerkarapidana implPerkarapidana;
    List<perkara_pidana> lb;

    public controllerPerkarapidana(skck_form_perkara_pidana frame) {
        this.frame = frame;
        implPerkarapidana = new daoPerkarapidana();
        lb = implPerkarapidana.getALL();
    }
    
    //merupakan fungsi untuk insert data berdasarkan inputan user dari textfield di frame
    public void insert() {
        
     // if (!frame.getj1().getText().trim().isEmpty()& !frame.getTxtj().getText().trim().isEmpty()) {
          
        perkara_pidana b = new perkara_pidana();
        b.setPerkara(frame.getj1().getText());
        b.setPutusan_vonis(frame.getj2().getText());
        b.setProsesper_kasus(frame.getj3().getText());
        b.setSejauh_mn_proses(frame.getj4().getText());
        b.setHukuman_norma_sosial(frame.getj5().getText());
        b.setProses_sejauh_mn(frame.getj6().getText());
       
        
        implPerkarapidana.insert(b);
        JOptionPane.showMessageDialog(null, "Simpan Data sukses");
        
        } //else {
            //JOptionPane.showMessageDialog(frame, "Data Tidak Boleh Kosong");
        //}
    }
