/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.controller;

import java.sql.Statement;
import javax.swing.JOptionPane;
import skck.model.databaseModel;
import skck.model.satwilModel;
import skck.view.skck_form_satwil;

/**
 *
 * @author Gannn
 */
public class satwilController {
    private databaseModel db;
    private satwilModel satwilModel;

    public satwilModel getSatwilModel() {
        return satwilModel;
    }

    public void setSatwilModel(satwilModel satwilModel) {
        this.satwilModel = satwilModel;
    }
    
    public void simpanForm(skck_form_satwil view) {
        String keperluan = view.getComboKeperluan().getSelectedItem().toString();
        String satwil = view.getComboSatwil().getSelectedItem().toString();
        String alamat = view.getTxtAlamat().getText();
        String provinsi = view.getComboProvinsi().getSelectedItem().toString();
        String kota = view.getComboKota().getSelectedItem().toString();
        String kec = view.getComboKec().getSelectedItem().toString();
        String kel = view.getComboKel().getSelectedItem().toString();

        if (alamat.trim().equals("")) {
            JOptionPane.showMessageDialog(view, "Alamat tidak boleh kosong!");
        } else {
            if(Insert(keperluan, satwil, alamat, provinsi, kota, kec, kel)){
                satwilModel.pindah();
            }
        }
    }
    
    public boolean Insert(String keperluan, String satwil,String alamat, String provinsi, String kota, String kec, String kel) {
        boolean result = false;
        Statement stmt = db.createStmt();
        String query = "INSERT INTO form_satwil(nama_keperluan,nama_satwil,alamat,provinsi,kota,kec,kel) VALUES('" + keperluan + "','" + satwil + "','" + alamat + "','" + provinsi + "','" + kota + "','" + kec + "','" + kel + "')";
        try {
            stmt.execute(query);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
