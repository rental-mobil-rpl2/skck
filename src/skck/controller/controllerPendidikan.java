/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.controller;

import skck.DAO.daoPendidikan;
import skck.DAOImplement.implementPendidikan;
import skck.model.pendidikan;
import skck.view.skck_form_pendidikan;
import java.util.List;
import javax.swing.JOptionPane;
/**
 *
 * @author Fajar
 */
public class controllerPendidikan {
    skck_form_pendidikan frame;
    implementPendidikan implPendidikan;
    List<pendidikan> lb;

    public controllerPendidikan(skck_form_pendidikan frame) {
        this.frame = frame;
        implPendidikan = new daoPendidikan();
        lb = implPendidikan.getALL();
    }
    
    //merupakan fungsi untuk insert data berdasarkan inputan user dari textfield di frame
    public void insert() {
        
     // if (!frame.getj1().getText().trim().isEmpty()& !frame.getTxtj().getText().trim().isEmpty()) {
          
        pendidikan b = new pendidikan();
        b.setTingkat((String) frame.getTxtTingkat().getSelectedItem());
        b.setUniv(frame.getTxtUniv().getText());
        b.setProvinsi((String) frame.getTxtPropinsi().getSelectedItem());
        b.setKabkota((String) frame.getTxtKabKota().getSelectedItem());
        b.setTahun(Integer.parseInt(frame.getTxtTahun().getText()));
        
        
        implPendidikan.insert(b);
        JOptionPane.showMessageDialog(null, "Simpan Data sukses");
        
        }
}
