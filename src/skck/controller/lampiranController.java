/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.controller;
import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Int;
import static java.lang.Integer.parseInt;
import javax.swing.JOptionPane;
import java.sql.*;
import javax.swing.JTable;
import skck.model.databaseModel;
import skck.view.skck_form_lampiran;
import skck.model.lampiranModel;

/**
 *
 * @author Gannn
 */
public class lampiranController {
    private databaseModel db;
    private lampiranModel lampiranModel;

    public lampiranModel getLampiranModel() {
        return lampiranModel;
    }

    public void setLampiranModel(lampiranModel lampiranModel) {
        this.lampiranModel = lampiranModel;
    }
    
    public void simpanForm(skck_form_lampiran view) {
        String ktp = view.getTxtKtp().getText();
        String paspor = view.getTxtPaspor().getText();
        String kk = view.getTxtKk().getText();
        String akte = view.getTxtAkte().getText();
        String jari = view.getTxtJari().getText();

        if (ktp.trim().equals("")) {
            JOptionPane.showMessageDialog(view, "KTP tidak boleh kosong!");
        } else if (paspor.equals("")) {
            JOptionPane.showMessageDialog(view, "PASPOR tidak boleh kosong!");
        } else if (kk.equals("")) {
            JOptionPane.showMessageDialog(view, "KK tidak boleh kosong!");
        } else if (akte.equals("")) {
            JOptionPane.showMessageDialog(view, "AKTE tidak boleh kosong!");
        } else if (jari.equals("")) {
            JOptionPane.showMessageDialog(view, "Sidik Jari tidak boleh kosong!");
        } else {
            if(Insert(ktp, paspor, kk, akte,jari)){
                lampiranModel.pindah();
            }
        }
    }
    
    public boolean Insert(String ktp, String paspor,String kk, String akte, String jari) {
        boolean result = false;
        Statement stmt = db.createStmt();
        String query = "INSERT INTO lampiran(ktp,paspor,kk,akte,jari) VALUES('" + ktp + "','" + paspor + "','" + kk + "','" + akte + "','" + jari + "')";
        try {
            stmt.execute(query);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
