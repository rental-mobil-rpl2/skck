/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.controller;

import static java.lang.Integer.parseInt;
import java.sql.Statement;
import javax.swing.JOptionPane;
import skck.model.databaseModel;
import skck.model.fisikModel;
import skck.view.skck_form_fisik;

/**
 *
 * @author Gannn
 */
public class fisikController {
    private databaseModel db;
    private fisikModel fisikModel;

    public fisikModel getFisikModel() {
        return fisikModel;
    }

    public void setFisikModel(fisikModel fisikModel) {
        this.fisikModel = fisikModel;
    }
    
    public void simpanForm(skck_form_fisik view) {
        String rambut = view.getTxtRambut().getText().toString();
        String wajah = view.getTxtWajah().getText().toString();
        String kulit = view.getTxtKulit().getText().toString();
        int tinggi = parseInt(view.getTxtTinggi().getText());
        int berat = parseInt(view.getTxtBerat().getText());
        String tanda = view.getTxtTanda().getText().toString();
        String kiri = view.getTxtKiri().getText().toString();
        String kanan = view.getTxtKanan().getText().toString();

        if (rambut.trim().equals("")) {
            JOptionPane.showMessageDialog(view, "Rambut tidak boleh kosong!");
        } else if (wajah.trim().equals("")) {
            JOptionPane.showMessageDialog(view, "Wajah tidak boleh kosong!");
        } else if (kulit.trim().equals("")) {
            JOptionPane.showMessageDialog(view, "Kulit tidak boleh kosong!");
        } else if (tanda.trim().equals("")) {
            JOptionPane.showMessageDialog(view, "Tanda tidak boleh kosong!");
        } else if (kiri.trim().equals("")) {
            JOptionPane.showMessageDialog(view, "Kiri tidak boleh kosong!");
        } else if (kanan.trim().equals("")) {
            JOptionPane.showMessageDialog(view, "Kanan tidak boleh kosong!");
        } else {
            if(Insert(rambut, wajah, kulit, tinggi, berat, tanda, kiri, kanan)){
                fisikModel.pindah();
            }
        }
    }
    
    public boolean Insert(String rambut, String wajah,String kulit, int tinggi, int berat, String tanda, String kiri, String kanan) {
        boolean result = false;
        Statement stmt = db.createStmt();
        String query = "INSERT INTO fisik(rambut,wajah,kulit,tinggi,berat,tanda,jari_kiri,jari_kanan) VALUES('" + rambut + "','" + wajah + "','" + kulit + "','" + tinggi + "','" + berat + "','" + tanda + "','" + kiri + "','" + kanan + "')";
        try {
            stmt.execute(query);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
