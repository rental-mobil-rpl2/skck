 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.controller;
import skck.DAO.daoDatadiri;
import skck.DAOImplement.implementDatadiri;
import skck.model.datadiri;
import skck.view.skck_form_data_diri;
import java.util.List;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
/**
 *
 * @author Akmal
 */
public class controllerDatadiri {
     skck_form_data_diri frame;
    implementDatadiri implDatadiri;
    List<datadiri> lb;

    public controllerDatadiri(skck_form_data_diri frame) {
        this.frame = frame;
        implDatadiri = new daoDatadiri();
        lb = implDatadiri.getALL();
    }

    //mengosongkan field
//    public void reset() {
////        frame.getTxtId_mobil().setText("");
//        frame.getTxtNo_plat().setText("");
//        frame.getTxtJenis().setSelectedItem("");
//        frame.getTxtMerek().setSelectedItem("");
//        frame.getTxtThn_buat().setText("");
//        frame.getTxtWarna().setText("");
//        frame.getTxtHarga().setText("");
//        frame.getTxtStatus().setText("");
//    }


    //merupakan fungsi untuk insert data berdasarkan inputan user dari textfield di frame
    public void insert() {
        
      if (!frame.getTxtNama().getText().trim().isEmpty()& !frame.getTxtNama().getText().trim().isEmpty()) {
          
        datadiri b = new datadiri();
        b.setNama(frame.getTxtNama().getText());
        b.setTempat_lahir(frame.getTxtTempat().getText());
        b.setTanggal_lahir(frame.getJKKKK().getText());
        b.setJenis_kelamin(frame.getJK().getText());
        b.setStatus_perkawinan(frame.getJKK().getText());
        b.setKewarganegaraan(frame.getJKKK().getText());
        b.setAgama((String) frame.getTxtAgama().getSelectedItem());
        b.setPekerjaan(frame.getTxtPekerjaan().getText());
        b.setNo_telp(frame.getTxtNotelp().getText());
        b.setAlamat(frame.getTxtAlamat().getText());
        b.setPropinsi((String) frame.getTxtPropinsi().getSelectedItem());
        b.setKabkota((String) frame.getTxtKabkota().getSelectedItem());
        b.setKecamatan((String) frame.getTxtKecamatan().getSelectedItem());
        b.setKelurahan((String) frame.getTxtKelurahan().getSelectedItem());
        b.setType_identitas((String) frame.getTxtTypeidentitas().getSelectedItem());
        b.setNo_identitas(frame.getTxtNoidentitas().getText());
        b.setNo_paspor(frame.getTxtNopaspor().getText());
        b.setFoto(frame.getTxtFoto().getText());
        
        implDatadiri.insert(b);
        JOptionPane.showMessageDialog(null, "Simpan Data sukses");
        
        } else {
            JOptionPane.showMessageDialog(frame, "Data Tidak Boleh Kosong");
        }
    }

    

    //berfungsi menghapus data yang dipilih
//    public void delete() {
//        if (!frame.getTxtID().getText().trim().isEmpty()) {
//            int id = Integer.parseInt(frame.getTxtID().getText());
//            implMobil.delete(id);
//            
//            JOptionPane.showMessageDialog(null, "Hapus Data  sukses");
//        } else {
//            JOptionPane.showMessageDialog(frame, "Pilih data yang akan di hapus");
//        }
//    }

//    public void isiTableCariNama() {
//        lb = implMahasiswa.getCariNama(frame.getTxtCariNama().getText());
//        tableModelMahasiswa tmb = new tableModelMahasiswa(lb);
//        frame.getTabelData().setModel(tmb);
//    }
//
//    public void carinama() {
//        if (!frame.getTxtCariNama().getText().trim().isEmpty()) {
//            implMahasiswa.getCariNama(frame.getTxtCariNama().getText());
//            isiTableCariNama();
//        } else {
//            JOptionPane.showMessageDialog(frame, "SILAHKAN PILIH DATA");
//        }
//    }
}
