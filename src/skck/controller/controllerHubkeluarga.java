 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.controller;
import skck.DAO.daoHubkeluarga;
import skck.DAOImplement.implementHubkeluarga;
import skck.model.hubkeluarga;
import skck.view.skck_form_hubkeluarga;
import java.util.List;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
/**
 *
 * @author Akmal
 */
public class controllerHubkeluarga {
     skck_form_hubkeluarga frame;
    implementHubkeluarga implHubkeluarga;
    List<hubkeluarga> lb;

    public controllerHubkeluarga(skck_form_hubkeluarga frame) {
        this.frame = frame;
        implHubkeluarga = new daoHubkeluarga();
        lb = implHubkeluarga.getALL();
    }

    //mengosongkan field
//    public void reset() {
////        frame.getTxtId_mobil().setText("");
//        frame.getTxtNo_plat().setText("");
//        frame.getTxtJenis().setSelectedItem("");
//        frame.getTxtMerek().setSelectedItem("");
//        frame.getTxtThn_buat().setText("");
//        frame.getTxtWarna().setText("");
//        frame.getTxtHarga().setText("");
//        frame.getTxtStatus().setText("");
//    }


    //merupakan fungsi untuk insert data berdasarkan inputan user dari textfield di frame
    public void insert() {
        
      if (!frame.getTxtNama1().getText().trim().isEmpty()& !frame.getTxtNama1().getText().trim().isEmpty()) {
          
        hubkeluarga b = new hubkeluarga();
        b.setHub((String)frame.getTxtHub().getSelectedItem());
        b.setNama1(frame.getTxtNama1().getText());
        b.setKewarganegaraan1(frame.getTxtKewarganegaraan1().getText());
        b.setAgama1((String) frame.getTxtAgama1().getSelectedItem());
        b.setPekerjaan1(frame.getTxtPekerjaan1().getText());
        b.setAlamat1(frame.getTxtAlamat1().getText());
        b.setPropinsi1((String) frame.getTxtPropinsi1().getSelectedItem());
        b.setKabkota1((String) frame.getTxtKabkota1().getSelectedItem());
        b.setKecamatan1((String) frame.getTxtKecamatan1().getSelectedItem());
        b.setKelurahan1((String) frame.getTxtKelurahan1().getSelectedItem());
        b.setNama2(frame.getTxtNama2().getText());
        b.setKewarganegaraan2(frame.getTxtKewarganegaraan2().getText());
        b.setAgama2((String) frame.getTxtAgama2().getSelectedItem());
        b.setPekerjaan2(frame.getTxtPekerjaan2().getText());
        b.setAlamat2(frame.getTxtAlamat2().getText());
        b.setPropinsi2((String) frame.getTxtPropinsi2().getSelectedItem());
        b.setKabkota2((String) frame.getTxtKabkota2().getSelectedItem());
        b.setKecamatan2((String) frame.getTxtKecamatan2().getSelectedItem());
        b.setKelurahan2((String) frame.getTxtKelurahan2().getSelectedItem());
        b.setNama3(frame.getTxtNama3().getText());
        b.setKewarganegaraan3(frame.getTxtKewarganegaraan3().getText());
        b.setAgama3((String) frame.getTxtAgama3().getSelectedItem());
        b.setPekerjaan3(frame.getTxtPekerjaan3().getText());
        b.setAlamat3(frame.getTxtAlamat3().getText());
        b.setPropinsi3((String) frame.getTxtPropinsi3().getSelectedItem());
        b.setKabkota3((String) frame.getTxtKabkota3().getSelectedItem());
        b.setKecamatan3((String) frame.getTxtKecamatan3().getSelectedItem());
        b.setKelurahan3((String) frame.getTxtKelurahan3().getSelectedItem());
        
        implHubkeluarga.insert(b);
        JOptionPane.showMessageDialog(null, "Simpan Data sukses");
        
        } else {
            JOptionPane.showMessageDialog(frame, "Data Tidak Boleh Kosong");
        }
    }

    

    //berfungsi menghapus data yang dipilih
//    public void delete() {
//        if (!frame.getTxtID().getText().trim().isEmpty()) {
//            int id = Integer.parseInt(frame.getTxtID().getText());
//            implMobil.delete(id);
//            
//            JOptionPane.showMessageDialog(null, "Hapus Data  sukses");
//        } else {
//            JOptionPane.showMessageDialog(frame, "Pilih data yang akan di hapus");
//        }
//    }

//    public void isiTableCariNama() {
//        lb = implMahasiswa.getCariNama(frame.getTxtCariNama().getText());
//        tableModelMahasiswa tmb = new tableModelMahasiswa(lb);
//        frame.getTabelData().setModel(tmb);
//    }
//
//    public void carinama() {
//        if (!frame.getTxtCariNama().getText().trim().isEmpty()) {
//            implMahasiswa.getCariNama(frame.getTxtCariNama().getText());
//            isiTableCariNama();
//        } else {
//            JOptionPane.showMessageDialog(frame, "SILAHKAN PILIH DATA");
//        }
//    }
}
