/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.model;

import javax.swing.JOptionPane;
import skck.event.fisikListener;
import skck.view.skck_form_fisik;
import skck.view.skck_form_lampiran;


/**
 *
 * @author Gannn
 */
public class fisikModel {
    private String rambut;
    private String wajah;
    private String kulit;
    private int tinggi;
    private int berat;
    private String tanda;
    private String jari_kiri;
    private String jari_kanan;
    
    private fisikListener fisikListener;

    public fisikListener getFisikListener() {
        return fisikListener;
    }

    public void setFisikListener(fisikListener fisikListener) {
        this.fisikListener = fisikListener;
    }

    public String getRambut() {
        return rambut;
    }

    public void setRambut(String rambut) {
        this.rambut = rambut;
        fireOnChange();
    }

    public String getWajah() {
        return wajah;
    }

    public void setWajah(String wajah) {
        this.wajah = wajah;
        fireOnChange();
    }

    public String getKulit() {
        return kulit;
    }

    public void setKulit(String kulit) {
        this.kulit = kulit;
        fireOnChange();
    }

    public int getTinggi() {
        return tinggi;
    }

    public void setTinggi(int tinggi) {
        this.tinggi = tinggi;
        fireOnChange();
    }

    public int getBerat() {
        return berat;
    }

    public void setBerat(int berat) {
        this.berat = berat;
        fireOnChange();
    }

    public String getTanda() {
        return tanda;
    }

    public void setTanda(String tanda) {
        this.tanda = tanda;
        fireOnChange();
    }

    public String getJari_kiri() {
        return jari_kiri;
    }

    public void setJari_kiri(String jari_kiri) {
        this.jari_kiri = jari_kiri;
        fireOnChange();
    }

    public String getJari_kanan() {
        return jari_kanan;
    }

    public void setJari_kanan(String jari_kanan) {
        this.jari_kanan = jari_kanan;
        fireOnChange();
    }
    
    protected void fireOnChange(){
        if(fisikListener != null){
            fisikListener.onChange(this);
        }
    }
    
    public void pindah(){
        new skck_form_fisik().dispose();
        new skck_form_lampiran().setVisible(true);
    }
    
}
