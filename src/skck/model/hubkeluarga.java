/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.model;

/**
 *
 * @author Akmal
 */
public class hubkeluarga {

    public Integer getId_hub() {
        return id_hub;
    }

    public void setId_hub(Integer id_hub) {
        this.id_hub = id_hub;
    }

    public String getHub() {
        return hub;
    }

    public void setHub(String hub) {
        this.hub = hub;
    }

    public String getNama1() {
        return nama1;
    }

    public void setNama1(String nama1) {
        this.nama1 = nama1;
    }

    public String getAgama1() {
        return agama1;
    }

    public void setAgama1(String agama1) {
        this.agama1 = agama1;
    }

    public String getKewarganegaraan1() {
        return kewarganegaraan1;
    }

    public void setKewarganegaraan1(String kewarganegaraan1) {
        this.kewarganegaraan1 = kewarganegaraan1;
    }

    public String getPekerjaan1() {
        return pekerjaan1;
    }

    public void setPekerjaan1(String pekerjaan1) {
        this.pekerjaan1 = pekerjaan1;
    }

    public String getAlamat1() {
        return alamat1;
    }

    public void setAlamat1(String alamat1) {
        this.alamat1 = alamat1;
    }

    public String getPropinsi1() {
        return propinsi1;
    }

    public void setPropinsi1(String propinsi1) {
        this.propinsi1 = propinsi1;
    }

    public String getKabkota1() {
        return kabkota1;
    }

    public void setKabkota1(String kabkota1) {
        this.kabkota1 = kabkota1;
    }

    public String getKecamatan1() {
        return kecamatan1;
    }

    public void setKecamatan1(String kecamatan1) {
        this.kecamatan1 = kecamatan1;
    }

    public String getKelurahan1() {
        return kelurahan1;
    }

    public void setKelurahan1(String kelurahan1) {
        this.kelurahan1 = kelurahan1;
    }

    public String getNama2() {
        return nama2;
    }

    public void setNama2(String nama2) {
        this.nama2 = nama2;
    }

    public String getAgama2() {
        return agama2;
    }

    public void setAgama2(String agama2) {
        this.agama2 = agama2;
    }

    public String getKewarganegaraan2() {
        return kewarganegaraan2;
    }

    public void setKewarganegaraan2(String kewarganegaraan2) {
        this.kewarganegaraan2 = kewarganegaraan2;
    }

    public String getPekerjaan2() {
        return pekerjaan2;
    }

    public void setPekerjaan2(String pekerjaan2) {
        this.pekerjaan2 = pekerjaan2;
    }

    public String getAlamat2() {
        return alamat2;
    }

    public void setAlamat2(String alamat2) {
        this.alamat2 = alamat2;
    }

    public String getPropinsi2() {
        return propinsi2;
    }

    public void setPropinsi2(String propinsi2) {
        this.propinsi2 = propinsi2;
    }

    public String getKabkota2() {
        return kabkota2;
    }

    public void setKabkota2(String kabkota2) {
        this.kabkota2 = kabkota2;
    }

    public String getKecamatan2() {
        return kecamatan2;
    }

    public void setKecamatan2(String kecamatan2) {
        this.kecamatan2 = kecamatan2;
    }

    public String getKelurahan2() {
        return kelurahan2;
    }

    public void setKelurahan2(String kelurahan2) {
        this.kelurahan2 = kelurahan2;
    }

    public String getNama3() {
        return nama3;
    }

    public void setNama3(String nama3) {
        this.nama3 = nama3;
    }

    public String getAgama3() {
        return agama3;
    }

    public void setAgama3(String agama3) {
        this.agama3 = agama3;
    }

    public String getKewarganegaraan3() {
        return kewarganegaraan3;
    }

    public void setKewarganegaraan3(String kewarganegaraan3) {
        this.kewarganegaraan3 = kewarganegaraan3;
    }

    public String getPekerjaan3() {
        return pekerjaan3;
    }

    public void setPekerjaan3(String pekerjaan3) {
        this.pekerjaan3 = pekerjaan3;
    }

    public String getAlamat3() {
        return alamat3;
    }

    public void setAlamat3(String alamat3) {
        this.alamat3 = alamat3;
    }

    public String getPropinsi3() {
        return propinsi3;
    }

    public void setPropinsi3(String propinsi3) {
        this.propinsi3 = propinsi3;
    }

    public String getKabkota3() {
        return kabkota3;
    }

    public void setKabkota3(String kabkota3) {
        this.kabkota3 = kabkota3;
    }

    public String getKecamatan3() {
        return kecamatan3;
    }

    public void setKecamatan3(String kecamatan3) {
        this.kecamatan3 = kecamatan3;
    }

    public String getKelurahan3() {
        return kelurahan3;
    }

    public void setKelurahan3(String kelurahan3) {
        this.kelurahan3 = kelurahan3;
    }

    private Integer id_hub;
    private String hub;
    private String nama1;
    private String agama1;
    private String kewarganegaraan1;
    private String pekerjaan1;
    private String alamat1;
    private String propinsi1;
    private String kabkota1;
    private String kecamatan1;
    private String kelurahan1;
    private String nama2;
    private String agama2;
    private String kewarganegaraan2;
    private String pekerjaan2;
    private String alamat2;
    private String propinsi2;
    private String kabkota2;
    private String kecamatan2;
    private String kelurahan2;
    private String nama3;
    private String agama3;
    private String kewarganegaraan3;
    private String pekerjaan3;
    private String alamat3;
    private String propinsi3;
    private String kabkota3;
    private String kecamatan3;
    private String kelurahan3;

}
