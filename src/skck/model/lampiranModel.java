/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.model;

import javax.swing.JOptionPane;
import skck.event.lampiranListener;
import skck.view.skck_form_keterangan;
import skck.view.skck_form_lampiran;

/**
 *
 * @author Gannn
 */
public class lampiranModel {
    private String ktp;
    private String paspor;
    private String kk;
    private String akte;
    private String jari;
    
    private lampiranListener lampiranListener;

    public lampiranListener getLampiranListener() {
        return lampiranListener;
    }

    public void setLampiranListener(lampiranListener lampiranListener) {
        this.lampiranListener = lampiranListener;
    }

    public String getKtp() {
        return ktp;
    }

    public void setKtp(String ktp) {
        this.ktp = ktp;
        fireOnChange();
    }

    public String getPaspor() {
        return paspor;
    }

    public void setPaspor(String paspor) {
        this.paspor = paspor;
        fireOnChange();
    }

    public String getKk() {
        return kk;
    }

    public void setKk(String kk) {
        this.kk = kk;
        fireOnChange();
    }

    public String getAkte() {
        return akte;
    }

    public void setAkte(String akte) {
        this.akte = akte;
        fireOnChange();
    }

    public String getJari() {
        return jari;
    }

    public void setJari(String jari) {
        this.jari = jari;
        fireOnChange();
    }
    
    protected void fireOnChange(){
        if(lampiranListener != null){
            lampiranListener.onChange(this);
        }
    }
    
    public void pindah(){
        new skck_form_lampiran().dispose();
        new skck_form_keterangan().setVisible(true);
    }
}
