/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.model;

import javax.swing.JOptionPane;
import skck.event.satwilListener;
import skck.view.skck_form_data_diri;
import skck.view.skck_form_satwil;

/**
 *
 * @author Gannn
 */
public class satwilModel {
    private String keperluan;
    private String satwil;
    private String alamat;
    private String provinsi;
    private String kota;
    private String kec;
    private String kel;
    
    private satwilListener satwilListener;

    public satwilListener getSatwilListener() {
        return satwilListener;
    }

    public void setSatwilListener(satwilListener satwilListener) {
        this.satwilListener = satwilListener;
    }

    public String getKeperluan() {
        return keperluan;
    }

    public void setKeperluan(String keperluan) {
        this.keperluan = keperluan;
        fireOnChange();
    }

    public String getSatwil() {
        return satwil;
    }

    public void setSatwil(String satwil) {
        this.satwil = satwil;
        fireOnChange();
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
        fireOnChange();
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
        fireOnChange();
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
        fireOnChange();
    }

    public String getKec() {
        return kec;
    }

    public void setKec(String kec) {
        this.kec = kec;
        fireOnChange();
    }

    public String getKel() {
        return kel;
    }

    public void setKel(String kel) {
        this.kel = kel;
        fireOnChange();
    }
    
    protected void fireOnChange(){
        if(satwilListener != null){
            satwilListener.onChange(this);
        }
    }
    
    public void pindah(){
        new skck_form_data_diri().setVisible(true);

    }
}
