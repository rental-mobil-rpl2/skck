/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.model;

/**
 *
 * @author Fajar
 */
public class perkara_pidana {
    
    private Integer id_per;
    private String perkara;
    private String putusan_vonis;
    private String prosesper_kasus;
    private String sejauh_mn_proses;
    private String hukuman_norma_sosial;
    private String proses_sejauh_mn;

    public Integer getId_per() {
        return id_per;
    }

    public void setId_per(Integer id_per) {
        this.id_per = id_per;
    }

    public String getPerkara() {
        return perkara;
    }

    public void setPerkara(String perkara) {
        this.perkara = perkara;
    }

    public String getPutusan_vonis() {
        return putusan_vonis;
    }

    public void setPutusan_vonis(String putusan_vonis) {
        this.putusan_vonis = putusan_vonis;
    }

    public String getProsesper_kasus() {
        return prosesper_kasus;
    }

    public void setProsesper_kasus(String prosesper_kasus) {
        this.prosesper_kasus = prosesper_kasus;
    }

    public String getSejauh_mn_proses() {
        return sejauh_mn_proses;
    }

    public void setSejauh_mn_proses(String sejauh_mn_proses) {
        this.sejauh_mn_proses = sejauh_mn_proses;
    }

    public String getHukuman_norma_sosial() {
        return hukuman_norma_sosial;
    }

    public void setHukuman_norma_sosial(String hukuman_norma_sosial) {
        this.hukuman_norma_sosial = hukuman_norma_sosial;
    }

    public String getProses_sejauh_mn() {
        return proses_sejauh_mn;
    }

    public void setProses_sejauh_mn(String proses_sejauh_mn) {
        this.proses_sejauh_mn = proses_sejauh_mn;
    }
}
